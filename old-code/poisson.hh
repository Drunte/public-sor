#ifndef DUNE_SOR_POISSON_HH
#define DUNE_SOR_POISSON_HH

#include <cassert>
#include <cmath>


#include <dune/istl/bcrsmatrix.hh>
#include <dune/istl/bvector.hh>

#include <iostream>

namespace Dune
{
  template <class Matrix>
  void setupMatrix(Matrix& A, const int dim = 2 )
  {
    assert( A.buildMode() == A::implicit );

    const double diag = std::pow( 2.0, dim);
    const double offdiag = -1.0;
    const size_t rows = A.N();
    const size_t k = size_t(std::sqrt(rows)); // assuming rows = N_x * N_y
    for(size_t i=0; i<rows; ++i )
    {
      A.entry( i,i ) = diag;
      if( i > 0 )
        A.entry( i-1, i ) = offdiag;
      if( i > k )
        A.entry( i-k, i ) = offdiag;

      if( i<rows-1 )
        A.entry( i+1, i ) = offdiag;
      if( i < rows-k-1 )
        A.entry( i+k, i ) = offdiag;
    }

    // finalize implicit build mode
    A.compress();

  /*
	auto endrow = A.end();
	for (auto row = A.begin(); row != endrow; ++row )
	{
		auto endcol = (*row).end();
		for (auto col = (*row).begin(); col != endcol; ++col) {
			std::cout << "(" << row.index() << "," << col.index() << "," << *col << ")" << std::endl;

		}
	}*/
  }
}

#endif
