




#include "jac_mpi_all.h"

#include "dune_pch.h"

#include <iostream>
using std::cout;
using std::endl;




int main(int argc, char** argv) {

	cout << "jacobi mpi started" << endl;

	//Dune::MPIHelper& helper = Dune::MPIHelper::instance(argc, argv);
	Dune::MPIHelper::instance(argc, argv);

	ptpComm comm;

	//cout << "i am rank " << comm.rank() << " of size " << comm.size() << endl;

	const size_t ncols = 4;
	const size_t nrows = ncols / comm.size();
	
	const size_t myrank = comm.rank();
	const size_t offset = myrank * nrows;

	Matrix A(nrows, ncols, 3, 1.0, Matrix::implicit);
	const double diag = 4.0;
	const double offdiag = 1.0;

	//set up A
	//diagonals first
	for (size_t i = 0; i < nrows; i++){
		A.entry(i, i + offset) = diag;
	}
	//super diag next
	for (size_t i = 0; i < nrows; i++){
		if (i + offset + 1 < ncols){
			A.entry(i, i + offset + 1) = offdiag;
		}
	}
	//sub diag finally
	for (size_t i = 0; i < nrows; i++){
		if (i + offset - 1 >= 0){
			A.entry(i, i + offset - 1) = offdiag;
		}
	}

	/*
	A.entry(0, offset) = diag;
	A.entry(1, offset + 1) = diag;

	A.entry(0, offset + 1) = offdiag;
	A.entry(1, offset) = offdiag;

	if (myrank==0){
		A.entry(1, 2) = offdiag;
	}
	else {
		A.entry(0, 1) = offdiag;
	}*/

	A.compress();

	Vector b(nrows);
	for (size_t i = 0; i < nrows; i++){
		b[i] = 1.0 + offset + i;

	}

	//cout << "rows: " << A.N() << " cols: " << A.M() << endl;
	

	Vector bfull(ncols);
	comm.barrier();
	comm.allgather(b.data(), b.size(), bfull.data());

	Vector x0(ncols);
	comm.barrier();
	const auto x = jac_mpi_all(A, b, x0, comm);
	comm.barrier();

	Vector xsol(ncols);
	comm.allgather(x.data(), x.size(), xsol.data());

	if (myrank == 0) {
		print(xsol);
	}

	const auto res = matvec_collect(A, xsol, comm);
	double res_norm = norm_of_diff_square(res, bfull);

	if (myrank==0){
		cout << "sanity check" << endl;
		print(res);
		cout << "residual norm is: " << res_norm << endl;
	}

	return 0;
}

















