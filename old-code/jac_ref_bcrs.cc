

#include <iostream>

#include "jac_ref_bcrs.h"

#include "dune_pch.h"

typedef Dune::BCRSMatrix< double > Matrix;
typedef Dune::BlockVector< double > Vector;


Vector jac_ref_bcrs(const Matrix &A, const Vector &b, const Vector &x, double tol ) {
	
	Vector previous{ x }; //we need 2 states for jacobi
	Vector next{ x }; //copy of initial guess

	int iter = 0; //iteration counter
	
	do {
		iter++;

		next.swap(previous); //exchange roles

		auto endrow = A.end();
		for (auto row = A.begin(); row != endrow; ++row) { //for each loop

			size_t rowindex = row.index();
			double sum = b[rowindex];                       // start sum with b[rowindex] to save 1 flop. 
			auto endcol = (*row).end();
			for (auto col = (*row).begin(); col != endcol; ++col) { //col here has both the index and the value

				size_t colindex = col.index();
				if (rowindex != colindex) {                    //skip diagonal

					sum -= (*col) * previous[colindex]; //(*col) is the value. use subtraction here to save 1 flop at the end.
				}
			}

			next[rowindex] = sum / A[rowindex][rowindex]; //update next iterate. A[r][r] is the diagonal element.
		}


	} while (norm_of_diff_square(next, previous) > tol*tol); //check for convergence, no need to take square roots

	std::cout << "iterations: " << iter << std::endl;

	return next; //the final iterate
}


