





#ifndef GS_REF_BCRS_H
#define GS_REF_BCRS_H


#include "dune_pch.h"

typedef Dune::BCRSMatrix< double > Matrix;
typedef Dune::BlockVector< double > Vector;

Vector gs_ref_bcrs(const Matrix &A, const Vector &b, const Vector &x, double tol = 1.e-6);


#endif // !GS_REF_BCRS_H





