



#include "matrix_setup_red_black.h"


#undef NDEBUG
#include <cassert>
#include <iostream>
using std::cout;
using std::endl;


int main() {

	cout << "testing red black mapping started" << endl;
	cout << "testcases taken from reference literature at pages 406-407 (note the indexing change from 1 to 0)" << endl;

	size_t xgrid = 6;
	size_t dim = xgrid * 4;

	assert(mapNaturalToRedBlackIndex(0, xgrid, dim) == 0);
	assert(mapNaturalToRedBlackIndex(1, xgrid, dim) == 12);
	assert(mapNaturalToRedBlackIndex(2, xgrid, dim) == 1);
	assert(mapNaturalToRedBlackIndex(3, xgrid, dim) == 13);
	assert(mapNaturalToRedBlackIndex(4, xgrid, dim) == 2);
	assert(mapNaturalToRedBlackIndex(5, xgrid, dim) == 14);
	assert(mapNaturalToRedBlackIndex(6, xgrid, dim) == 15);
	assert(mapNaturalToRedBlackIndex(7, xgrid, dim) == 3);
	assert(mapNaturalToRedBlackIndex(8, xgrid, dim) == 16);
	assert(mapNaturalToRedBlackIndex(9, xgrid, dim) == 4);
	assert(mapNaturalToRedBlackIndex(10, xgrid, dim) == 17);
	assert(mapNaturalToRedBlackIndex(11, xgrid, dim) == 5);
	assert(mapNaturalToRedBlackIndex(12, xgrid, dim) == 6);
	assert(mapNaturalToRedBlackIndex(13, xgrid, dim) == 18);
	assert(mapNaturalToRedBlackIndex(14, xgrid, dim) == 7);
	assert(mapNaturalToRedBlackIndex(15, xgrid, dim) == 19);
	assert(mapNaturalToRedBlackIndex(16, xgrid, dim) == 8);
	assert(mapNaturalToRedBlackIndex(17, xgrid, dim) == 20);
	assert(mapNaturalToRedBlackIndex(18, xgrid, dim) == 21);
	assert(mapNaturalToRedBlackIndex(19, xgrid, dim) == 9);
	assert(mapNaturalToRedBlackIndex(20, xgrid, dim) == 22);
	assert(mapNaturalToRedBlackIndex(21, xgrid, dim) == 10);
	assert(mapNaturalToRedBlackIndex(22, xgrid, dim) == 23);
	assert(mapNaturalToRedBlackIndex(23, xgrid, dim) == 11);

	cout << "block 1 clear with xgrid: " << xgrid << " and dim: " << dim << endl;

	xgrid = 9;
	dim = 45;

	assert(mapNaturalToRedBlackIndex(0, xgrid, dim) == 0);
	assert(mapNaturalToRedBlackIndex(2, xgrid, dim) == 1);
	assert(mapNaturalToRedBlackIndex(4, xgrid, dim) == 2);
	assert(mapNaturalToRedBlackIndex(44, xgrid, dim) == 22);
	assert(mapNaturalToRedBlackIndex(43, xgrid, dim) == 44);
	assert(mapNaturalToRedBlackIndex(42, xgrid, dim) == 21);
	assert(mapNaturalToRedBlackIndex(21, xgrid, dim) == 33);
	assert(mapNaturalToRedBlackIndex(17, xgrid, dim) == 31);
	assert(mapNaturalToRedBlackIndex(18, xgrid, dim) == 9);
	assert(mapNaturalToRedBlackIndex(26, xgrid, dim) == 13);
	assert(mapNaturalToRedBlackIndex(31, xgrid, dim) == 38);	

	cout << "block 2 clear with xgrid: " << xgrid << " and dim: " << dim << endl;

	xgrid = 9;
	dim = 36;

	assert(mapNaturalToRedBlackIndex(0, xgrid, dim) == 0);
	assert(mapNaturalToRedBlackIndex(2, xgrid, dim) == 1);
	assert(mapNaturalToRedBlackIndex(4, xgrid, dim) == 2);
	assert(mapNaturalToRedBlackIndex(9, xgrid, dim) == 22);
	assert(mapNaturalToRedBlackIndex(19, xgrid, dim) == 27);
	assert(mapNaturalToRedBlackIndex(35, xgrid, dim) == 35);
	assert(mapNaturalToRedBlackIndex(34, xgrid, dim) == 17);
	assert(mapNaturalToRedBlackIndex(33, xgrid, dim) == 34);
	assert(mapNaturalToRedBlackIndex(17, xgrid, dim) == 26);
	assert(mapNaturalToRedBlackIndex(22, xgrid, dim) == 11);

	cout << "block 3 clear with xgrid: " << xgrid << " and dim: " << dim << endl;

	xgrid = 10;
	dim = 50;

	assert(mapNaturalToRedBlackIndex(0, xgrid, dim) == 0);
	assert(mapNaturalToRedBlackIndex(2, xgrid, dim) == 1);
	assert(mapNaturalToRedBlackIndex(4, xgrid, dim) == 2);
	assert(mapNaturalToRedBlackIndex(9, xgrid, dim) == 29);
	assert(mapNaturalToRedBlackIndex(10, xgrid, dim) == 30);
	assert(mapNaturalToRedBlackIndex(49, xgrid, dim) == 49);
	assert(mapNaturalToRedBlackIndex(47, xgrid, dim) == 48);
	assert(mapNaturalToRedBlackIndex(20, xgrid, dim) == 10);
	assert(mapNaturalToRedBlackIndex(19, xgrid, dim) == 9);
	assert(mapNaturalToRedBlackIndex(30, xgrid, dim) == 40);
	assert(mapNaturalToRedBlackIndex(29, xgrid, dim) == 39);
	assert(mapNaturalToRedBlackIndex(39, xgrid, dim) == 19);
	assert(mapNaturalToRedBlackIndex(11, xgrid, dim) == 5);
	assert(mapNaturalToRedBlackIndex(25, xgrid, dim) == 37);

	cout << "block 4 clear with xgrid: " << xgrid << " and dim: " << dim << endl;

	cout << "all tests clear!" << endl;


	/*
	xgrid = 10;
	dim = 100;

	for (size_t i = 0; i < dim; i++) {
		cout << mapNaturalToRedBlackIndex(i, xgrid, dim) << endl;

	}*/
}











