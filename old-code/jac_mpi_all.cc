



#include <iostream>

#include "jac_mpi_all.h"

#include "dune_pch.h"

using std::cout;
using std::endl;

typedef Dune::BCRSMatrix< double > Matrix;
typedef Dune::BlockVector< double > Vector;

typedef Dune::SimpleMessageBuffer MsgBuffer;
typedef Dune::Point2PointCommunicator< MsgBuffer > ptpComm;



Vector jac_mpi_all(const Matrix &A, const Vector &b, const Vector &x, ptpComm &comm, double tol) {

	Vector previous( x.size() ); //long. needed for matvec
	Vector next( b.size() ); //short. only few rows

	//initialize next to keep the initial guess
	const size_t offset = comm.rank() * b.size();
	for (size_t i = 0; i < b.size(); i++){
		next[i] = x[i + offset];
	}
	/*
	cout << "next: ";
	print(next);
	cout << "prev: ";
	print(previous);
	*/

	int iter = 0;
	double termination_criteria;
	do{
		iter++;

		//cout << "iteration: " << iter << endl;

		//copy stage
		comm.allgather(next.data(), next.size(), previous.data());

		auto endrow = A.end();
		for (auto row = A.begin(); row != endrow; ++row) {

			const size_t rowindex = row.index();
			double sum = b[rowindex];
			auto endcol = (*row).end();
			for (auto col = (*row).begin(); col != endcol; ++col) {

				const size_t colindex = col.index();
				if (rowindex + offset != colindex) { //this was the last error

					sum -= (*col) * previous[colindex];
				}
			}

			next[rowindex] = sum / A[rowindex][rowindex + offset]; //diag? might need offset
		}

		double partial = norm_of_diff_square(comm, next, previous);
		termination_criteria = comm.sum(partial);

		//cout << "termination crit: " << termination_criteria << endl;

		

	} while (termination_criteria > tol*tol);



	std::cout << "iterations: " << iter << std::endl;
	return next;
}














