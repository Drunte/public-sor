




#include "jac_mpi_ptp.h"
#include "gs_mpi.h"
#include "sor_mpi.h"

#include "dune_pch.h"

#include "bundle.h"

#include "matrix_setup_red_black.h"

#include <chrono>
#include <thread>

#include <cmath>

#include <vector>
#include <set>
#include <string>
#include <iostream>
using std::cout;
using std::endl;
using std::stoi;
using std::set;







void printmsg(const std::string& name, const double resnorm, const double maxtime, const double maxcomm, const double maxcopy) {
	cout << endl;
	cout << "residual norm " << name << " is: " << resnorm << endl;
	cout << "greatest wallclock time for " << name << ": " << maxtime << endl;
	cout << "greatest wallclock time spent communicating for " << name << ": " << maxcomm << endl;
	cout << "% of time spent communicating for " << name << ": " << 100 * maxcomm / maxtime << "%" << endl;
	cout << "greatest wallclock time spent copying for " << name << ": " << maxcopy << endl;
	cout << "% of time spent copying for " << name << ": " << 100 * maxcopy / maxtime << "%" << endl;

}

const double pi = M_PI;
double rhs(double x, double y) {
	return sin(pi*x) * sin(pi*y);
}



int main(int argc, char** argv) {

	Dune::MPIHelper::instance(argc, argv);

	if (argc != 2){
		cout << "missing commandline argument: need exactly 1 unsigned int >= 2, representing the gridsize" << endl;
		exit(1);
	}

	ptpComm comm;
	const size_t myrank = comm.rank();
	const size_t commsize = comm.size();
	const size_t lastrank = commsize - 1;
	const size_t firstrank = 0;

	

	const int input = stoi(argv[1]);

	if (input < 2) {
		cout << "input must be positive and >= 2. aborting... " << endl;
		exit(1);
	}

	if (input % commsize != 0) {
		if (myrank == firstrank) {
			cout << "process number: " << commsize << " does not divide gridsize: " << input << " evenly! aborting... " << endl;
		}
		exit(2);
	}

	if (myrank == firstrank) {
		cout << "SOR started with " << commsize << " processes and gridsize " << input << endl;
	}

	const size_t xgrid = input;
	const size_t ygrid = input;
	const size_t cols = xgrid * ygrid;
	const size_t realrows = cols / commsize; 
	const size_t ghostrowsnr = commsize == 1 ? 0 : (myrank == firstrank || myrank == lastrank) ? xgrid : 2 * xgrid; //amount of ghostrows
	
	const size_t dim = realrows + ghostrowsnr;

	//linkage setup
	setupLinkageByRows(comm);

	//ghostrows setup
	std::vector<size_t> ghostrows;
	setupGhostRowsRB(comm, ghostrows, xgrid, dim);

	std::vector<bool> ghostbools(dim, true);
	for (auto r : ghostrows) {		
		ghostbools[r] = false;
	}

	//send and recv links
	std::vector<std::vector<size_t>> sendindicesVec;
	std::vector<std::vector<size_t>> recvindicesVec;
	setupSendRecvIndecisRB(comm, sendindicesVec, recvindicesVec, xgrid, dim);

	//bundle setup
	Dune::Timer timer(false);
	Bundle bundle(comm, timer, xgrid, ghostrows, ghostbools, sendindicesVec, recvindicesVec);

	//matrix setup
	Matrix A(dim, dim, 5, 1.0, Matrix::implicit); 
	setupMatrixRedBlack(A, ghostbools, xgrid, dim);

	//initial guess setup
	Vector initialguess(dim);
	
	//const size_t targetvalue = myrank * realrows - xgrid;
	//const size_t nonLeadingGhostRowsCompensator = myrank == firstrank ? xgrid : 0;

	// /*
	const double dx = 1.0 / (xgrid); //xgrid is number of interior points
	const double dy = 1.0 / ygrid;
	const double dx2=dx/2;
	const double dy2=dy/2;
	const size_t offset = myrank * ygrid / commsize;
	const size_t compensator = myrank == firstrank ? 0 : -1;
	// */
	
	Vector b(dim);
	//target vector setup
	for (size_t i = 0; i < dim; i++) {
		size_t mapped_i = mapNaturalToRedBlackIndex(i, xgrid, dim);

		const double x = (i % xgrid) * dx+dx2;
		const double y = (i / ygrid + offset + compensator) * dy+dy2;

		b[mapped_i] = dx * dy * rhs(x, y);
		
		//b[mapped_i] = i + targetvalue + nonLeadingGhostRowsCompensator;
	}

	//const double tol = 1.e-6 * std::sqrt(norm_square_with_ignore(b, ghostbools));
	//cout << "tol= " << tol << endl;
	const double tol = 1.e-6;

	//call algorithm
	double relax = 1.28;
	//Dune::Timer timer;
	timer.start();
	auto x = jac_mpi_ptp(A, b, initialguess, bundle);
	double timeelapsed = timer.elapsed();
	//timer.stop();
	double timecommjac = bundle.timeSpentCommunicating;
	bundle.timeSpentCommunicating = 0.0;
	double timecopyjac = bundle.timeSpentCopying;
	bundle.timeSpentCopying = 0.0;
	//double timeelapsed = timer.elapsed();
	timer.reset();
	auto x2 = gs_mpi(A, b, initialguess, bundle, tol);
	double timeelapsed2 = timer.elapsed();
	//timer.stop();
	double timecommgs = bundle.timeSpentCommunicating;
	bundle.timeSpentCommunicating = 0.0;
	double timecopygs = bundle.timeSpentCopying;
	bundle.timeSpentCopying = 0.0;
	//double timeelapsed2 = timer.elapsed();
	timer.reset();
	auto x3 = sor_mpi(A, b, initialguess, bundle, relax);
	double timeelapsed3 = timer.elapsed();
	//timer.stop();
	double timecommsor = bundle.timeSpentCommunicating;
	bundle.timeSpentCommunicating = 0.0;
	double timecopysor = bundle.timeSpentCopying;
	bundle.timeSpentCopying = 0.0;
	//double timeelapsed3 = timer.elapsed();

	Vector y(dim);
	Vector y2(dim);
	Vector y3(dim);
	//sanity check
	A.mv(x,y);
	A.mv(x2, y2);
	A.mv(x3, y3);
	

	double partial = norm_of_diff_square(b, y, ghostbools); // all b y and x should have zeros corresponding to the ghost rows
	double resnorm = comm.sum(partial);

	double partial2 = norm_of_diff_square(b, y2, ghostbools);
	double resnorm2 = comm.sum(partial2);

	double partial3 = norm_of_diff_square(b, y3, ghostbools);
	double resnorm3 = comm.sum(partial3);

	//std::this_thread::sleep_for(std::chrono::milliseconds(30));
	double delta = timeelapsed2;// -timeelapsed;
	double delta2 = timeelapsed3;// -timeelapsed2;
	double maxtime = comm.max(timeelapsed);
	double max_gs = comm.max(delta);
	double max_sor = comm.max(delta2);
	double max_comm_jac = comm.max(timecommjac);
	double max_comm_gs = comm.max(timecommgs);
	double max_comm_sor = comm.max(timecommsor);
	double max_copy_jac = comm.max(timecopyjac);
	double max_copy_gs = comm.max(timecopygs);
	double max_copy_sor = comm.max(timecopysor);
	if (myrank == firstrank) {
		printmsg("JAC", resnorm, maxtime, max_comm_jac, max_copy_jac);
		printmsg("GS", resnorm2, max_gs, max_comm_gs, max_copy_gs);
		printmsg("SOR", resnorm3, max_sor, max_comm_sor, max_copy_sor);
	}

	return 0;
}

















