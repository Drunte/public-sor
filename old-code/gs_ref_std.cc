



#include <iostream>

#include "gs_ref_std.h"
#include "lin_alg_util.h"

#include "dune_pch.h"


using std::vector;

using Dune::BCRSMatrix;

vector<double> gs_ref_std(const vector<vector<double>> &A, const vector<double> &b, const vector<double> &x, double tol = 1.e-6) {
	//placeholder types
	vector<double> previous = x;
	vector<double> next = x;

	int iter = 0;
	do{
		iter++;
		for (size_t i = 0; i < x.size(); i++){
			previous[i] = next[i];
		}

		for (size_t row = 0; row < x.size(); row++){

			double sum = b[row];
			for (size_t col = 0; col < x.size(); col++){

				if (row != col) {
					sum -= A[row][col] * next[col];
				}
			}
			next[row] = sum / A[row][row];
		}
	} while (norm_2(sub(next, previous)) > tol);

	std::cout << "iterations: " << iter << std::endl;

	return next;
}




