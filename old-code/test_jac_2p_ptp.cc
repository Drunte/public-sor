




#include "jac_mpi_ptp.h"

#include "dune_pch.h"

#include "bundle.h"

#include <chrono>
#include <thread>



#include <vector>
#include <set>
#include <string>
#include <iostream>
using std::cout;
using std::endl;
using std::stoi;
using std::set;





int main(int argc, char** argv) {

	Dune::MPIHelper::instance(argc, argv);

	

	ptpComm comm;
	const size_t myrank = comm.rank();
	const size_t commsize = comm.size();
	const size_t lastrank = commsize - 1;
	const size_t firstrank = 0;

	if (commsize != 2){

		cout << "run with exactly 2 processes" << endl;
		exit(1);
	}

	if (myrank == 0) {
		cout << "jacobi 2 processes started with " << commsize << " processes" << endl;
	}

	const size_t xgrid = 10;
	const size_t ygrid = 10;
	const size_t cols = xgrid * ygrid;
	const size_t realrows = cols / commsize; // assume 2 processes
	const size_t ghostrowsnr = (myrank == firstrank || myrank == lastrank) ? xgrid : 2 * xgrid;
	const size_t dim = realrows + ghostrowsnr;
	

	set<int> sendlinks;
	set<int> recvlinks;

	sendlinks.insert((myrank + 1) % commsize);
	recvlinks.insert((myrank + 1) % commsize);

	comm.insertRequest(sendlinks, recvlinks);
	//comm.printLinkage();


	std::vector<size_t> ghostrows;
	if (myrank == firstrank) {
		ghostrows = { 50,51,52,53,54,55,56,57,58,59 }; //needs to be generalized
	}
	else {
		ghostrows = { 0,1,2,3,4,5,6,7,8,9 };
	}

	std::vector<bool> ghostbools(dim, true);
	for (auto r : ghostrows) {		
		ghostbools[r] = false;
	}

	//send and recv links
	std::vector<size_t> sendindices;
	std::vector<size_t> recvindices;
	if (myrank == 0) {
		sendindices = { 40,41,42,43,44,45,46,47,48,49 };  //also needs to be generalized
		recvindices = { 50,51,52,53,54,55,56,57,58,59 };
	}
	else {
		sendindices = { 10,11,12,13,14,15,16,17,18,19 };
		recvindices = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
	}

	std::vector<std::vector<size_t>> sendindicesVec{ sendindices };
	std::vector<std::vector<size_t>> recvindicesVec{ recvindices };

	Bundle bundle(comm, ghostrows, ghostbools, sendindicesVec, recvindicesVec);



	Matrix A(dim, dim, 5, 1.0, Matrix::implicit); 
	const double diag = 4.0;
	const double offdiag = -1.0;


	const size_t start = 0; 
	const size_t end = start + dim;
	
	for (size_t i = start; i < end; i++){
		if (ghostbools[i]) {
			A.entry(i, i) = diag;

			if (i + 1 < dim && (i + 1) % xgrid > 0) {
				A.entry(i, i + 1) = offdiag;
			}

			if (i + xgrid < dim) {
				A.entry(i, i + xgrid) = offdiag;
			}

			if (i > 0 && i % xgrid > 0) {
				A.entry(i, i - 1) = offdiag;
			}

			if (i >= xgrid) {
				A.entry(i, i - xgrid) = offdiag;
			}
		}
		else{
			A.entry(i, i) = 1.0;
		}
	}	

	A.compress();

	Vector initialguess(dim);

	const size_t targetvalue = myrank == firstrank ? 0 : realrows - xgrid;
	
	Vector b(dim);

	for (size_t i = start; i < end; i++) {
		if (!ghostbools[i]) {
			b[i] = 0.0;
		}
		else{
			b[i] = i + targetvalue;
		}
	}
	//print(b);
	auto x = jac_mpi_ptp(A, b, initialguess, bundle);

	Vector y(dim);

	A.mv(x,y);

	for (const auto& gr:ghostrows){
		y[gr] = 0.0;

	}

	/*

	if (myrank==0) {
		//print(x);
		cout << "   process " << myrank << endl;
		cout << "   solution x: " << endl;
		print(x);
		cout << "   target vector b: " << endl;
		print(b);
		cout << "      Ax: " << endl;
		print(y);
		//cout << "A: " << endl;
		//print(A);
		std::this_thread::sleep_for(std::chrono::milliseconds(30));
	}

	comm.barrier();

	if (myrank==1){

		cout << "   process " << myrank << endl;
		cout << "   solution x: " << endl;
		print(x);
		cout << "   target vector b: " << endl;
		print(b);
		cout << "      Ax: " << endl;
		print(y);
		//cout << "A: " << endl;
		//print(A);
		std::this_thread::sleep_for(std::chrono::milliseconds(30));
	}

	comm.barrier();

	*/

	double partial = norm_of_diff_square(b, y); // all b y and x should have zeros corresponding to the ghost rows
	double resnorm = comm.sum(partial);

	//std::this_thread::sleep_for(std::chrono::milliseconds(30));
	if (myrank == firstrank) {
		cout << "residual norm is: " << resnorm << endl;
	}

	return 0;
}





















