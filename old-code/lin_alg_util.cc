


#include "lin_alg_util.h"
#include <vector>
#include <cmath>

using std::vector;
using std::sqrt;

double norm_2(const vector<double> &x) {

	double sum = 0.0;
	for (const double& d : x) {
		sum += d*d;
	}

	sum = sqrt(sum);

	return sum;
}


vector<double> sub(const vector<double> &left_operator, const vector<double> &right_operator) {

	vector<double> output(left_operator.size());

	for (size_t i = 0; i < left_operator.size(); i++) {
		output[i] = left_operator[i] - right_operator[i];
	}

	return output;
}




vector<double> add(const vector<double> &left_operator, const vector<double> &right_operator) {

	vector<double> output(left_operator.size());

	for (size_t i = 0; i < left_operator.size(); i++) {

		output[i] = left_operator[i] + right_operator[i];
	}

	return output;
}


double dot(const vector<double> &left_operator, const vector<double> &right_operator) {

	double sum = 0.0;

	for (size_t i = 0; i < left_operator.size(); i++){

		sum += left_operator[i] * right_operator[i];
	}

	return sum;
}




vector<double> mat_vec(const vector<vector<double>> &A, const vector<double> &x) {

	vector<double> output(x.size());

	for (size_t i = 0; i < x.size(); i++){

		output[i] = dot(A[i], x);
	}

	return output;
}


















