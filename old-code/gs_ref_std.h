






#ifndef GS_REF_STD_H
#define GS_REF_STD_H

#include <vector>

using std::vector;

vector<double> gs_ref_std(const vector<vector<double>> &A, const vector<double> &b, const vector<double> &x, double tol);


#endif // !GS_REF_H









