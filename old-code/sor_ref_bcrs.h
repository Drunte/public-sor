





#ifndef SOR_REF_BCRS_H
#define SOR_REF_BCRS_H


#include "dune_pch.h"

typedef Dune::BCRSMatrix< double > Matrix;
typedef Dune::BlockVector< double > Vector;

Vector sor_ref_bcrs(const Matrix &A, const Vector &b, const Vector &x, double relaxation_factor = 1.10, double tol = 1.e-6);


#endif // !SOR_REF_BCRS_H





