




#include "jac_mpi_ptp.h"
#include "gs_mpi.h"

#include "dune_pch.h"

#include "bundle.h"

#include "matrix_setup_red_black.h"

#include <chrono>
#include <thread>



#include <vector>
#include <set>
#include <string>
#include <iostream>
using std::cout;
using std::endl;
using std::stoi;
using std::set;





int main(int argc, char** argv) {

	Dune::MPIHelper::instance(argc, argv);

	if (argc != 2){
		cout << "missing commandline argument: need exactly 1 unsigned int >= 2, representing the gridsize" << endl;
		exit(1);
	}

	ptpComm comm;
	const size_t myrank = comm.rank();
	const size_t commsize = comm.size();
	const size_t lastrank = commsize - 1;
	const size_t firstrank = 0;

	if (myrank == firstrank) {
		cout << "Gauss-Seidel started with " << commsize << " processes" << endl;
	}

	const int input = stoi(argv[1]);

	if (input < 2) {
		cout << "input must be positive and >= 2. aborting... " << endl;
		exit(1);
	}

	if (input % commsize != 0) {
		if (myrank == firstrank) {
			cout << "process number: " << commsize << " does not divide gridsize: " << input << " evenly! aborting... " << endl;
		}
		exit(2);
	}

	const size_t xgrid = input;
	const size_t ygrid = input;
	const size_t cols = xgrid * ygrid;
	const size_t realrows = cols / commsize; 
	const size_t ghostrowsnr = commsize == 1 ? 0 : (myrank == firstrank || myrank == lastrank) ? xgrid : 2 * xgrid; //amount of ghostrows
	
	const size_t dim = realrows + ghostrowsnr;


	setupLinkageByRows(comm);

	//ghostrows setup
	std::vector<size_t> ghostrows;
	setupGhostRowsRB(comm, ghostrows, xgrid, dim);
	

	std::vector<bool> ghostbools(dim, true);
	for (auto r : ghostrows) {		
		ghostbools[r] = false;
	}

	//send and recv links
	std::vector<std::vector<size_t>> sendindicesVec;
	std::vector<std::vector<size_t>> recvindicesVec;
	setupSendRecvIndecisRB(comm, sendindicesVec, recvindicesVec, xgrid, dim);

	//bundle setup
	Dune::Timer timer(false);
	Bundle bundle(comm, timer, xgrid, ghostrows, ghostbools, sendindicesVec, recvindicesVec);


	Matrix A(dim, dim, 5, 1.0, Matrix::implicit); 
	setupMatrixRedBlack(A, ghostbools, xgrid, dim);

	//initial guess setup
	Vector initialguess(dim);
	
	const size_t targetvalue = myrank * realrows - xgrid;
	const size_t nonLeadingGhostRowsCompensator = myrank == firstrank ? xgrid : 0;
	
	Vector b(dim);
	//target vector setup
	for (size_t i = 0; i < dim; i++) {
		size_t mapped_i = mapNaturalToRedBlackIndex(i, xgrid, dim);
		if (!ghostbools[mapped_i]) {
			b[mapped_i] = 0.0;
		}
		else{
			b[mapped_i] = i + targetvalue + nonLeadingGhostRowsCompensator;
		}
	}

	//call algorithm
	//Dune::Timer timer;
	timer.start();
	auto x = jac_mpi_ptp(A, b, initialguess, bundle);
	double timeelapsed = timer.elapsed();
	auto x2 = gs_mpi(A, b, initialguess, bundle);
	double timeelapsed2 = timer.elapsed();

	Vector y(dim);
	Vector y2(dim);
	//sanity check
	A.mv(x,y);
	A.mv(x2, y2);

	for (const auto& gr:ghostrows){
		y[gr] = 0.0;
		y2[gr] = 0.0;
	}

	double partial = norm_of_diff_square(b, y); // all b y and x should have zeros corresponding to the ghost rows
	double resnorm = comm.sum(partial);

	double partial2 = norm_of_diff_square(b, y2);
	double resnorm2 = comm.sum(partial2);

	//std::this_thread::sleep_for(std::chrono::milliseconds(30));
	double delta = timeelapsed2 - timeelapsed;
	double maxtime = comm.max(timeelapsed);
	double max_gs = comm.max(delta);
	if (myrank == firstrank) {
		cout << "residual norm jac is: " << resnorm << endl;
		cout << "residual norm gs is: " << resnorm2 << endl;
		cout << "greatest time for jac: " << maxtime << endl;
		cout << "greatest time for GS: " << max_gs << endl;
	}

	return 0;
}





















