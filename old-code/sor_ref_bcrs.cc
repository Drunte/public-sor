

#include <iostream>

#include "sor_ref_bcrs.h"
//#include "lin_alg_util.h"

#include "dune_pch.h"

#include <cassert>

typedef Dune::BCRSMatrix< double > Matrix;
typedef Dune::BlockVector< double > Vector;


Vector sor_ref_bcrs(const Matrix &A, const Vector &b, const Vector &x, double relaxation_factor, double tol) {
	
	Vector previous{ x };//(x.size()); //zeros
	Vector next{ x }; //copy of initial guess

	std::cout << "relax factor: " << relaxation_factor << std::endl;

	int iter = 0;
	
	do {
		iter++;
		/*
		for (size_t i = 0; i < b.size(); i++) {
			previous[i] = next[i]; //swap
		}*/

		next.swap(previous);

		auto endrow = A.end();
		for (auto row = A.begin(); row != endrow; ++row) {

			size_t rowindex = row.index();
			double sum = b[rowindex];
			auto endcol = (*row).end();
			for (auto col = (*row).begin(); col != endcol; ++col) {

				size_t colindex = col.index();
				if (rowindex > colindex) {

					sum -= (*col) * next[colindex];
				}
				else if (rowindex < colindex){

					sum -= (*col) * previous[colindex];
				}
			}

			assert(A[rowindex][rowindex].infinity_norm() > 1.e-12);
			//next[rowindex] += relaxation_factor * (sum / A[rowindex][rowindex] - next[rowindex]);
			next[rowindex] = previous[rowindex] + relaxation_factor * (sum / A[rowindex][rowindex] - previous[rowindex]);
		}


	} while (norm_of_diff_square(next, previous) > tol*tol);
	



	std::cout << "iterations: " << iter << std::endl;

	return next;
}


