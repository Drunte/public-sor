

#include <iostream>

#include "jac_ref_std.h"
#include "lin_alg_util.h"

//#include "dune_pch.h"

using std::vector;


vector<double> jac_ref_std(const vector<vector<double>> &A, const vector<double> &b, const vector<double> &x, double tol = 1.e-6) {
	//placeholder types
	//placeholder algorithm for full matrix
	vector<double> previous{ x };//(x.size()); //zeros
	vector<double> next{ x }; //copy of initial guess

	int iter = 0;
	do { // at least 1 iteration
		iter++;
		//previous = next; // copy. this is needed from previous iteration
		for (size_t i = 0; i < x.size(); i++){
			previous[i] = next[i];

		}

		for (size_t row = 0; row < x.size(); row++){

			double sum = b[row];
			for (size_t col = 0; col < x.size(); col++){

				if (row != col){

					sum -= A[row][col] * previous[col];
				}
			}

			next[row] = sum / A[row][row];
						
		}
	
	} while (norm_2(sub(next,previous)) > tol); //Cauchy criteria || xk+1 - xk ||2 < tol

	std::cout << "iterations: " << iter << std::endl;

	return next;
}


