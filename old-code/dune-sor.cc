// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:

#ifdef HAVE_CONFIG_H
# include "config.h"
#endif
#include <iostream>
#include <dune/common/parallel/mpihelper.hh> // An initializer of MPI
#include <dune/common/parallel/mpicommunication.hh>
#include <dune/common/exceptions.hh> // We use exceptions

#include "poisson.hh"

#include "comm/p2pcommunicator.hh"
#include "comm.hh"


void algorithm ()
{
  typedef Dune::BCRSMatrix< double >  Matrix;
  typedef Dune::BlockVector< double > Vector;

  size_t N = 10;
  size_t rows = N * N;
  size_t nnzPerRow = 5 ;

  Matrix A( rows, rows, nnzPerRow, 1.0, Matrix::implicit ) ;
  Dune::setupMatrix( A );
  Vector b( rows );
 // Dune::setupRightHandSide( b );
}

int main(int argc, char** argv)
{
  try{
    // Maybe initialize MPI
    Dune::MPIHelper& helper = Dune::MPIHelper::instance(argc, argv);

    // implemented in p2pcommunicator
    typedef Dune::SimpleMessageBuffer MsgBuffer;
    Dune::Point2PointCommunicator< MsgBuffer > comm; // mpiComm extracted from MPIHelper

    std::cout << "Hello World! This is dune-sor." << std::endl;
    if(Dune::MPIHelper::isFake)
      std::cout<< "This is a sequential program." << std::endl;
    else
      std::cout<<"I am rank "<<helper.rank()<<" of "<<helper.size()
        <<" processes!"<<std::endl;

    algorithm();
	comm.barrier();
	//testComm(comm);

    testCollectives( comm );

    setupLinkage( comm );

    testPoint2Point( comm );

    return 0;
  }
  catch (Dune::Exception &e){
    std::cerr << "Dune reported error: " << e << std::endl;
  }
  catch (...){
    std::cerr << "Unknown exception thrown!" << std::endl;
  }
}
