




#include "jac_mpi_ptp.h"

#include "dune_pch.h"

#include "bundle.h"

#include "matrix_setup_natural_order.h"
#include "domain_setup.h"

#include <chrono>
#include <thread>



#include <vector>
//#include <set>
#include <string>
#include <iostream>
using std::cout;
using std::endl;
using std::stoi;
//using std::set;





int main(int argc, char** argv) {

	Dune::MPIHelper::instance(argc, argv);

	if (argc != 2){
		cout << "missing commandline argument: need exactly 1 unsigned int >= 2, representing the gridsize" << endl;
		exit(1);
	}

	ptpComm comm;
	const size_t myrank = comm.rank();
	const size_t commsize = comm.size();
	const size_t lastrank = commsize - 1;
	const size_t firstrank = 0;

	if (myrank == firstrank) {
		cout << "jacobi many processes started with " << commsize << " processes" << endl;
	}

	const int input = stoi(argv[1]);

	if (input < 2) {
		cout << "input must be positive and >= 2. aborting... " << endl;
		exit(1);
	}

	if (input % commsize != 0) {
		if (myrank == firstrank) {
			cout << "process number: " << commsize << " does not divide gridsize: " << input << " evenly! aborting... " << endl;
		}
		exit(2);
	}

	const size_t xgrid = input;
	const size_t ygrid = input;
	const size_t cols = xgrid * ygrid;
	const size_t realrows = cols / commsize;
	const size_t ghostrowsnr = commsize == 1 ? 0 : (myrank == firstrank || myrank == lastrank) ? xgrid : 2 * xgrid; //amount of ghostrows

	const size_t dim = realrows + ghostrowsnr;

	//send and recv links
	std::vector<std::vector<size_t>> sendindicesVec;
	std::vector<std::vector<size_t>> recvindicesVec;
	//setupSendRecvIndecis(comm, sendindicesVec, recvindicesVec, xgrid, dim);
	setupSendRecvIndices< 2 >(comm, xgrid, sendindicesVec, recvindicesVec);

  //std::abort();


	//setup linkage
	//setupLinkageByRows(comm);

	//ghostrows setup
	std::vector<size_t> ghostrows;
	setupGhostRows(comm, ghostrows, xgrid, dim);

	std::vector<bool> ghostbools(dim, true);
	for (auto r : ghostrows) {
		ghostbools[r] = false;
	}


	//bundle setup
	Dune::Timer timer(false);
	Bundle bundle(comm, timer, xgrid, ghostrows, ghostbools, sendindicesVec, recvindicesVec);


	Matrix A(dim, dim, 5, 1.0, Matrix::implicit);
	setupMatrixNaturalOrder(A, ghostbools, xgrid, dim);


	//initial guess setup
	Vector initialguess(dim);

	const size_t targetvalue = myrank * realrows - xgrid;
	const size_t nonLeadingGhostRowsCompensator = myrank == firstrank ? xgrid : 0;

	Vector b(dim);
	//target vector setup
	for (size_t i = 0; i < dim; i++) {
		if (!ghostbools[i]) {
			b[i] = 0.0;
		}
		else{
			b[i] = i + targetvalue + nonLeadingGhostRowsCompensator;
		}
	}

	//call algorithm
	//Dune::Timer timer;
	timer.start();
	auto x = jac_mpi_ptp(A, b, initialguess, bundle);
	double timeelapsed = timer.elapsed();

	Vector y(dim);
	//sanity check
	A.mv(x,y);

	for (const auto& gr:ghostrows){
		y[gr] = 0.0;
	}

	double partial = norm_of_diff_square(b, y); // all b y and x should have zeros corresponding to the ghost rows
	double resnorm = comm.sum(partial);

	//std::this_thread::sleep_for(std::chrono::milliseconds(30));
	double maxtime = comm.max(timeelapsed);
	if (myrank == firstrank) {
		cout << "residual norm is: " << resnorm << endl;
		cout << "greatest time: " << maxtime << endl;
	}

	return 0;
}





















