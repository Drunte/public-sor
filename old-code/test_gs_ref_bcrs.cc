




#include "gs_ref_bcrs.h"
//#include "lin_alg_util.h"

#include "dune_pch.h"

#include <iostream>
using std::cout;
using std::endl;

int main() {

	//python ref solution: 0.16, 0.35, 0.44, 0.89
	cout << "Gauss-Seidel bcrs started\n" << endl;

	
	Matrix A(4, 4, 3, 1.0, Matrix::implicit);
	const double diag = 4.0;
	const double offdiag = 1.0;

	A.entry(0, 0) = diag;
	A.entry(1, 1) = diag;
	A.entry(2, 2) = diag;
	A.entry(3, 3) = diag;

	A.entry(0, 1) = offdiag;
	A.entry(1, 0) = offdiag;
	A.entry(1, 2) = offdiag;
	A.entry(2, 1) = offdiag;
	A.entry(2, 3) = offdiag;
	A.entry(3, 2) = offdiag;

	A.compress();

	Vector b{ 1,2,3,4 };
	Vector x0(4);

	//const double tol = 1.e-6;
	const auto x = gs_ref_bcrs(A, b, x0);//, tol);

	cout << "result:" << endl;

	for (const auto &xs:x) {
		cout << xs << ", ";
	}
	cout << endl;

	cout << "should be:\n0.16, 0.35, 0.44, 0.89" << endl;

	
	//const auto xsol = mat_vec(A, x);
	Vector xsol(4);
	A.mv(x, xsol);

	cout << "sanity check" << endl;

	for (const auto &xs : xsol) {
		cout << xs << ", ";
	}
	cout << endl;

	cout << "should be:\n";
	for (const auto &bs : b) {
		cout << bs << ", ";
	}
	cout << endl;

	

	return 0;
}





