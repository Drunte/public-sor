


#ifndef LIN_ALG_UTIL_H
#define LIN_ALG_UTIL_H
#include <vector>
using std::vector;

double norm_2(const vector<double> &x);

vector<double> sub(const vector<double> &left_operator, const vector<double> &right_operator);

vector<double> add(const vector<double> &left_operator, const vector<double> &right_operator);

double dot(const vector<double> &left_operator, const vector<double> &right_operator);

vector<double> mat_vec(const vector<vector<double>> &A, const vector<double> &x);

#endif // !LIN_ALG_UTIL_H












