#ifndef DUNE_SOR_COMM_HH
#define DUNE_SOR_COMM_HH

#include <set>
#include <cmath>

#include <dune/common/parallel/communication.hh>
#include <dune/common/parallel/mpicommunication.hh>

namespace Dune
{

  template <class Comm>
  void testCollectives( const Comm& comm )
  {
    std::cout << "I'm rank " << comm.rank() << " of " << comm.size() << std::endl;

    double r = comm.rank();
    double sum = comm.sum( r );
    double min = comm.min( r );
    double max = comm.max( r );

    std::cout << "P[ " << comm.rank() << "] sum = " << sum << std::endl;
    std::cout << "P[ " << comm.rank() << "] min = " << min << std::endl;
    std::cout << "P[ " << comm.rank() << "] max = " << max << std::endl;
  }

  template <class Comm>
  void setupLinkage( Comm& comm )
  {
    // setup linkage (who talks to whom)
    std::set<int> sendLinks;
    std::set<int> recvLinks;

    // setup ring (only to be done once)
    int rank = comm.rank();
    sendLinks.insert( (rank+1)%comm.size() );
    recvLinks.insert( (rank == 0) ? comm.size()-1 : rank-1 );

    // setup linkage structure
    comm.insertRequest( sendLinks, recvLinks );

    comm.printLinkage();
  }


  template <class Comm>
  void testPoint2Point( const Comm& comm )
  {
    typedef typename Comm::MessageBufferType MessageBufferType;

    // get number of links (i.e. number of processes we are sending to)
    const int sendLinks = comm.sendLinks();
    const int me = comm.rank();

    std::vector< MessageBufferType > buffer( sendLinks );

    for( int l=0; l<sendLinks; ++l )
    {
      for( int i=0; i<4; ++i )
      {
        // write something to buffer of link l
        double val = std::pow( i+0.3, (me+i));
        buffer[ l ].write( val );
      }
    }

    // send buffer and store received messages in buffer (overwrite)
    std::vector< MessageBufferType > bu2 = comm.exchange( buffer );

    const int recvLinks = comm.recvLinks();
    for( int l=0; l<recvLinks; ++l )
    {
      for( int i=0; i<4; ++i )
      {
        double val;
        bu2[ l ].read( val );
        std::cout << "P[ " << me << " ] received " << val << std::endl;
      }
    }
  }

}
#endif
