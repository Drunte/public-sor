




#ifndef JAC_MPI_ALL_H
#define JAC_MPI_ALL_H

#include "dune_pch.h"


typedef Dune::BCRSMatrix< double > Matrix;
typedef Dune::BlockVector< double > Vector;

typedef Dune::SimpleMessageBuffer MsgBuffer;
typedef Dune::Point2PointCommunicator< MsgBuffer > ptpComm;


Vector jac_mpi_all(const Matrix &A, const Vector &b, const Vector &x, ptpComm &comm, double tol = 1.e-6);



#endif // !JAC_MPI_ALL_H


















