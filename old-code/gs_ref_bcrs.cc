

#include <iostream>

#include "gs_ref_bcrs.h"
//#include "lin_alg_util.h"

#include "dune_pch.h"

typedef Dune::BCRSMatrix< double > Matrix;
typedef Dune::BlockVector< double > Vector;


Vector gs_ref_bcrs(const Matrix &A, const Vector &b, const Vector &x, double tol) {
	
	Vector previous{ x };//(x.size()); //zeros
	Vector next{ x }; //copy of initial guess

	int iter = 0;
	
	do {
		iter++;
		/*
		for (size_t i = 0; i < b.size(); i++) {
			previous[i] = next[i];
		}
		*/

		next.swap(previous);
		

		auto endrow = A.end();
		for (auto row = A.begin(); row != endrow; ++row) {

			size_t rowindex = row.index();
			double sum = b[rowindex];
			auto endcol = (*row).end();
			for (auto col = (*row).begin(); col != endcol; ++col) {

				size_t colindex = col.index();
				if (rowindex < colindex) {

					sum -= (*col) * previous[colindex];
				}
				}
			//comm step
			for (auto col = (*row).begin(); col != endcol; ++col) {
				size_t colindex = col.index();
				if (rowindex > colindex){

					sum -= (*col) * next[colindex];
				}
			}

			next[rowindex] = sum / A[rowindex][rowindex];
		}


		//} while ((sub(next,previous)).two_norm() > tol);
	} while (norm_of_diff_square(next, previous) > tol*tol);
	



	std::cout << "iterations: " << iter << std::endl;

	return next;
}


