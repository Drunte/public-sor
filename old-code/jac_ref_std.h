





#ifndef JAC_REF_STD_H
#define JAC_REF_STD_H

//#include "dune_precompiled_header.h"


#include <vector>

using std::vector;

vector<double> jac_ref_std(const vector<vector<double>> &A, const vector<double> &b, const vector<double> &x, double tol);


#endif // !JAC_REF_H





