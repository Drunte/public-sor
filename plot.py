# -*- coding: utf-8 -*-
"""
Created on Mon Sep 27 13:39:57 2021

@author: henke
"""
from numpy import*
from scipy import*
from scipy.linalg import*
from matplotlib.pyplot import*



#names=['jac_row','jac_dom','GS_row','GS_dom','SOR_row','SOR_dom']
names=['JR','JD','GR','GD','SR','SD']

locs=[1,2,3,4,5,6]

patterns=["\\","/","+","-",".","*"]

figsize=(12,9)

##################
# exp1

figure(figsize=figsize)

vals1=[4.22,4.05,3.27,3.36,2.00,2.07]
vals10=[0.92,0.76,0.53,0.54,0.34,0.32]
vals25=[1.31,1.36,0.86,0.80,0.55,0.47]
vals50=[1.66,1.69,1.29,1.12,0.86,0.66]


subplot(221)
title("1 process")
ylabel("execution time in seconds(s)")
for i in range(6):   
    mask=[0]*6
    mask[i]=vals1[i]
    bar(locs,mask,tick_label=names,align='center',color='white',edgecolor='black',hatch=patterns[i])


subplot(222)
title("10 processes")
ylabel("execution time in seconds(s)")
#bar(locs,vals10,tick_label=names,align='center')
for i in range(6):   
    mask=[0]*6
    mask[i]=vals10[i]
    bar(locs,mask,tick_label=names,align='center',color='white',edgecolor='black',hatch=patterns[i])

subplot(223)
title("25 processes")
ylabel("execution time in seconds(s)")
#bar(locs,vals25,tick_label=names,align='center')
for i in range(6):   
    mask=[0]*6
    mask[i]=vals25[i]
    bar(locs,mask,tick_label=names,align='center',color='white',edgecolor='black',hatch=patterns[i])

subplot(224)
title("50 processes")
ylabel("execution time in seconds(s)")
#bar(locs,vals50,tick_label=names,align='center')
for i in range(6):   
    mask=[0]*6
    mask[i]=vals50[i]
    bar(locs,mask,tick_label=names,align='center',color='white',edgecolor='black',hatch=patterns[i])

suptitle("execution times for reduced version and grid size 100 on POWER8")




figure(figsize=figsize)

iter1=[14740,14740,8087,8087,4429,4429]
iter10=[14719,14740,8428,8333,4810,4700]
iter25=[14735,14740,8959,8438,5384,4815]
iter50=[14739,14740,9820,8613,6315,5006]

subplot(221)
title("1 process")
ylabel("iteration count")
#bar(locs,iter1,tick_label=names,align='center')
for i in range(6):
    mask=[0]*6
    mask[i]=iter1[i]
    bar(locs,mask,tick_label=names,align='center',color='white',edgecolor='black',hatch=patterns[i])

subplot(222)
title("10 processes")
ylabel("iteration count")
#bar(locs,iter10,tick_label=names,align='center')
for i in range(6):
    mask=[0]*6
    mask[i]=iter10[i]
    bar(locs,mask,tick_label=names,align='center',color='white',edgecolor='black',hatch=patterns[i])

subplot(223)
title("25 processes")
ylabel("iteration count")
#bar(locs,iter25,tick_label=names,align='center')
for i in range(6):
    mask=[0]*6
    mask[i]=iter25[i]
    bar(locs,mask,tick_label=names,align='center',color='white',edgecolor='black',hatch=patterns[i])

subplot(224)
title("50 processes")
ylabel("iteration count")
#bar(locs,iter50,tick_label=names,align='center')
for i in range(6):
    mask=[0]*6
    mask[i]=iter50[i]
    bar(locs,mask,tick_label=names,align='center',color='white',edgecolor='black',hatch=patterns[i])

suptitle("iteration counts for reduced version and grid size 100 on POWER8")




figure(figsize=figsize)



vals1=[213,137,112,114,69.2,71.9]
vals10=[17.6,16.0,13.8,13.5,8.63,8.10]
vals25=[21.4,19.8,17.0,14.7,10.5,8.51]
vals50=[24.2,20.7,19.5,15.2,12.3,8.95]


subplot(221)
title("1 process")
ylabel("execution time in seconds(s)")
for i in range(6):   
    mask=[0]*6
    mask[i]=vals1[i]
    bar(locs,mask,tick_label=names,align='center',color='white',edgecolor='black',hatch=patterns[i])


subplot(222)
title("10 processes")
ylabel("execution time in seconds(s)")
#bar(locs,vals10,tick_label=names,align='center')
for i in range(6):   
    mask=[0]*6
    mask[i]=vals10[i]
    bar(locs,mask,tick_label=names,align='center',color='white',edgecolor='black',hatch=patterns[i])

subplot(223)
title("25 processes")
ylabel("execution time in seconds(s)")
#bar(locs,vals25,tick_label=names,align='center')
for i in range(6):   
    mask=[0]*6
    mask[i]=vals25[i]
    bar(locs,mask,tick_label=names,align='center',color='white',edgecolor='black',hatch=patterns[i])

subplot(224)
title("50 processes")
ylabel("execution time in seconds(s)")
#bar(locs,vals50,tick_label=names,align='center')
for i in range(6):   
    mask=[0]*6
    mask[i]=vals50[i]
    bar(locs,mask,tick_label=names,align='center',color='white',edgecolor='black',hatch=patterns[i])

suptitle("execution times for reduced version and grid size 250 on POWER8")




figure(figsize=figsize)

iter1=[79339,79339,44095,44095,24416,24416]
iter10=[79212,79339,44783,44622,25223,25005]
iter25=[79311,79339,45958,44848,26498,25256]
iter50=[79332,79339,47831,45223,28564,25674]

subplot(221)
title("1 process")
ylabel("iteration count")
#bar(locs,iter1,tick_label=names,align='center')
for i in range(6):
    mask=[0]*6
    mask[i]=iter1[i]
    bar(locs,mask,tick_label=names,align='center',color='white',edgecolor='black',hatch=patterns[i])

subplot(222)
title("10 processes")
ylabel("iteration count")
#bar(locs,iter10,tick_label=names,align='center')
for i in range(6):
    mask=[0]*6
    mask[i]=iter10[i]
    bar(locs,mask,tick_label=names,align='center',color='white',edgecolor='black',hatch=patterns[i])

subplot(223)
title("25 processes")
ylabel("iteration count")
#bar(locs,iter25,tick_label=names,align='center')
for i in range(6):
    mask=[0]*6
    mask[i]=iter25[i]
    bar(locs,mask,tick_label=names,align='center',color='white',edgecolor='black',hatch=patterns[i])

subplot(224)
title("50 processes")
ylabel("iteration count")
#bar(locs,iter50,tick_label=names,align='center')
for i in range(6):
    mask=[0]*6
    mask[i]=iter50[i]
    bar(locs,mask,tick_label=names,align='center',color='white',edgecolor='black',hatch=patterns[i])

suptitle("iteration counts for reduced version and grid size 250 on POWER8")




figure(figsize=figsize)

vals10=[220,202,176,177,112,108]
vals25=[243,235,199,187,122,110]
vals50=[241,208,200,177,124,115]

subplot(131)
title("10 processes")
ylabel("execution time in seconds(s)")
#bar(locs,vals10,tick_label=names,align='center')
for i in range(6):
    mask=[0]*6
    mask[i]=vals10[i]
    bar(locs,mask,tick_label=names,align='center',color='white',edgecolor='black',hatch=patterns[i])

subplot(132)
title("25 processes")
ylabel("execution time in seconds(s)")
#bar(locs,vals25,tick_label=names,align='center')
for i in range(6):
    mask=[0]*6
    mask[i]=vals25[i]
    bar(locs,mask,tick_label=names,align='center',color='white',edgecolor='black',hatch=patterns[i])

subplot(133)
title("50 processes")
ylabel("execution time in seconds(s)")
#bar(locs,vals50,tick_label=names,align='center')
for i in range(6):
    mask=[0]*6
    mask[i]=vals50[i]
    bar(locs,mask,tick_label=names,align='center',color='white',edgecolor='black',hatch=patterns[i])

suptitle("execution times for reduced version and grid size 500 on POWER8")




figure(figsize=figsize)

iter10=[280332,280840,159117,158976,89769,89451]
iter25=[280728,280840,161299,159373,92108,89900]
iter50=[280810,280840,164632,160035,95835,90647]


subplot(131)
title("10 processes")
ylabel("iteration count")
#bar(locs,iter10,tick_label=names,align='center')
for i in range(6):
    mask=[0]*6
    mask[i]=iter10[i]
    bar(locs,mask,tick_label=names,align='center',color='white',edgecolor='black',hatch=patterns[i])

subplot(132)
title("25 processes")
ylabel("iteration count")
#bar(locs,iter25,tick_label=names,align='center')
for i in range(6):
    mask=[0]*6
    mask[i]=iter25[i]
    bar(locs,mask,tick_label=names,align='center',color='white',edgecolor='black',hatch=patterns[i])

subplot(133)
title("50 processes")
ylabel("iteration count")
#bar(locs,iter50,tick_label=names,align='center')
for i in range(6):
    mask=[0]*6
    mask[i]=iter50[i]
    bar(locs,mask,tick_label=names,align='center',color='white',edgecolor='black',hatch=patterns[i])

suptitle("iteration counts for reduced version and grid size 500 on POWER8")



###########################################################












###########################################################
#  exp2



figure(figsize=figsize)

vals1=[4.22,4.05,3.30,3.10,2.01,1.94]
vals10=[0.92,0.91,0.61,0.62,0.37,0.37]
vals25=[1.23,1.39,0.97,1.09,0.55,0.61]
vals50=[1.73,1.69,1.46,1.44,0.80,0.81]


subplot(221)
title("1 process")
ylabel("execution time in seconds(s)")
for i in range(6):   
    mask=[0]*6
    mask[i]=vals1[i]
    bar(locs,mask,tick_label=names,align='center',color='white',edgecolor='black',hatch=patterns[i])


subplot(222)
title("10 processes")
ylabel("execution time in seconds(s)")
#bar(locs,vals10,tick_label=names,align='center')
for i in range(6):   
    mask=[0]*6
    mask[i]=vals10[i]
    bar(locs,mask,tick_label=names,align='center',color='white',edgecolor='black',hatch=patterns[i])

subplot(223)
title("25 processes")
ylabel("execution time in seconds(s)")
#bar(locs,vals25,tick_label=names,align='center')
for i in range(6):   
    mask=[0]*6
    mask[i]=vals25[i]
    bar(locs,mask,tick_label=names,align='center',color='white',edgecolor='black',hatch=patterns[i])

subplot(224)
title("50 processes")
ylabel("execution time in seconds(s)")
#bar(locs,vals50,tick_label=names,align='center')
for i in range(6):   
    mask=[0]*6
    mask[i]=vals50[i]
    bar(locs,mask,tick_label=names,align='center',color='white',edgecolor='black',hatch=patterns[i])

suptitle("execution times for full version and grid size 100 on POWER8")




figure(figsize=figsize)

iter1=[14740,14740,8087,8087,4429,4429]
iter10=[14719,14740,8091,8277,4440,4640]
iter25=[14735,14740,8087,8187,4431,4541]
iter50=[14739,14740,8087,8152,4430,4501]

subplot(221)
title("1 process")
ylabel("iteration count")
#bar(locs,iter1,tick_label=names,align='center')
for i in range(6):
    mask=[0]*6
    mask[i]=iter1[i]
    bar(locs,mask,tick_label=names,align='center',color='white',edgecolor='black',hatch=patterns[i])

subplot(222)
title("10 processes")
ylabel("iteration count")
#bar(locs,iter10,tick_label=names,align='center')
for i in range(6):
    mask=[0]*6
    mask[i]=iter10[i]
    bar(locs,mask,tick_label=names,align='center',color='white',edgecolor='black',hatch=patterns[i])

subplot(223)
title("25 processes")
ylabel("iteration count")
#bar(locs,iter25,tick_label=names,align='center')
for i in range(6):
    mask=[0]*6
    mask[i]=iter25[i]
    bar(locs,mask,tick_label=names,align='center',color='white',edgecolor='black',hatch=patterns[i])

subplot(224)
title("50 processes")
ylabel("iteration count")
#bar(locs,iter50,tick_label=names,align='center')
for i in range(6):
    mask=[0]*6
    mask[i]=iter50[i]
    bar(locs,mask,tick_label=names,align='center',color='white',edgecolor='black',hatch=patterns[i])

suptitle("iteration counts for full version and grid size 100  on POWER8")







figure(figsize=figsize)

vals1=[213,137,112,105,70.0,66.5]
vals10=[17.8,16.0,15.1,13.5,9.69,8.51]
vals25=[21.7,20.2,19.1,16.6,11.1,9.65]
vals50=[24.4,20.1,26.0,17.6,16.8,10.6]


subplot(221)
title("1 process")
ylabel("execution time in seconds(s)")
for i in range(6):   
    mask=[0]*6
    mask[i]=vals1[i]
    bar(locs,mask,tick_label=names,align='center',color='white',edgecolor='black',hatch=patterns[i])


subplot(222)
title("10 processes")
ylabel("execution time in seconds(s)")
#bar(locs,vals10,tick_label=names,align='center')
for i in range(6):   
    mask=[0]*6
    mask[i]=vals10[i]
    bar(locs,mask,tick_label=names,align='center',color='white',edgecolor='black',hatch=patterns[i])

subplot(223)
title("25 processes")
ylabel("execution time in seconds(s)")
#bar(locs,vals25,tick_label=names,align='center')
for i in range(6):   
    mask=[0]*6
    mask[i]=vals25[i]
    bar(locs,mask,tick_label=names,align='center',color='white',edgecolor='black',hatch=patterns[i])

subplot(224)
title("50 processes")
ylabel("execution time in seconds(s)")
#bar(locs,vals50,tick_label=names,align='center')
for i in range(6):   
    mask=[0]*6
    mask[i]=vals50[i]
    bar(locs,mask,tick_label=names,align='center',color='white',edgecolor='black',hatch=patterns[i])

suptitle("execution times for full version and grid size 250 on POWER8")




figure(figsize=figsize)

iter1=[79339,79339,44095,44095,24416,24416]
iter10=[79212,79339,45503,44200,26025,24535]
iter25=[79311,79339,44086,44306,24415,24653]
iter50=[79332,79339,51512,45670,32607,26172]

subplot(221)
title("1 process")
ylabel("iteration count")
#bar(locs,iter1,tick_label=names,align='center')
for i in range(6):
    mask=[0]*6
    mask[i]=iter1[i]
    bar(locs,mask,tick_label=names,align='center',color='white',edgecolor='black',hatch=patterns[i])

subplot(222)
title("10 processes")
ylabel("iteration count")
#bar(locs,iter10,tick_label=names,align='center')
for i in range(6):
    mask=[0]*6
    mask[i]=iter10[i]
    bar(locs,mask,tick_label=names,align='center',color='white',edgecolor='black',hatch=patterns[i])

subplot(223)
title("25 processes")
ylabel("iteration count")
#bar(locs,iter25,tick_label=names,align='center')
for i in range(6):
    mask=[0]*6
    mask[i]=iter25[i]
    bar(locs,mask,tick_label=names,align='center',color='white',edgecolor='black',hatch=patterns[i])

subplot(224)
title("50 processes")
ylabel("iteration count")
#bar(locs,iter50,tick_label=names,align='center')
for i in range(6):
    mask=[0]*6
    mask[i]=iter50[i]
    bar(locs,mask,tick_label=names,align='center',color='white',edgecolor='black',hatch=patterns[i])

suptitle("iteration counts for full version and grid size 250  on POWER8")






figure(figsize=figsize)

vals10=[221,202,181,173,114,114]
vals25=[245,233,207,191,125,112]
vals50=[238,217,217,191,125,111]

subplot(131)
title("10 processes")
ylabel("execution time in seconds(s)")
#bar(locs,vals10,tick_label=names,align='center')
for i in range(6):
    mask=[0]*6
    mask[i]=vals10[i]
    bar(locs,mask,tick_label=names,align='center',color='white',edgecolor='black',hatch=patterns[i])

subplot(132)
title("25 processes")
ylabel("execution time in seconds(s)")
#bar(locs,vals25,tick_label=names,align='center')
for i in range(6):
    mask=[0]*6
    mask[i]=vals25[i]
    bar(locs,mask,tick_label=names,align='center',color='white',edgecolor='black',hatch=patterns[i])

subplot(133)
title("50 processes")
ylabel("execution time in seconds(s)")
#bar(locs,vals50,tick_label=names,align='center')
for i in range(6):
    mask=[0]*6
    mask[i]=vals50[i]
    bar(locs,mask,tick_label=names,align='center',color='white',edgecolor='black',hatch=patterns[i])

suptitle("execution times for full version and grid size 500 on POWER8")




figure(figsize=figsize)



iter10=[280332,280840,157846,158762,88332,89210]
iter25=[280728,280840,158001,158417,88383,88820]
iter50=[280810,280840,158036,158284,88396,88669]

subplot(131)
title("10 processes")
ylabel("iteration count")
#bar(locs,iter10,tick_label=names,align='center')
for i in range(6):
    mask=[0]*6
    mask[i]=iter10[i]
    bar(locs,mask,tick_label=names,align='center',color='white',edgecolor='black',hatch=patterns[i])

subplot(132)
title("25 processes")
ylabel("iteration count")
#bar(locs,iter25,tick_label=names,align='center')
for i in range(6):
    mask=[0]*6
    mask[i]=iter25[i]
    bar(locs,mask,tick_label=names,align='center',color='white',edgecolor='black',hatch=patterns[i])

subplot(133)
title("50 processes")
ylabel("iteration count")
#bar(locs,iter50,tick_label=names,align='center')
for i in range(6):
    mask=[0]*6
    mask[i]=iter50[i]
    bar(locs,mask,tick_label=names,align='center',color='white',edgecolor='black',hatch=patterns[i])

suptitle("iteration counts for full version and grid size 500  on POWER8")





################################################
#exp 3



figure(figsize=figsize)

vals1=[2900,2780,4677,4665,4810,4660]
vals10=[2233,2323,4193,4039,4185,4083]
vals25=[2241,2331,4046,3873,4078,3898]
vals50=[2429,2489,4164,3661,4323,3867]


subplot(221)
title("1 process")
ylabel("execution time in milliseconds(ms)")
#bar(locs,vals1,tick_label=names,align='center')
for i in range(6):
    mask=[0]*6
    mask[i]=vals1[i]
    bar(locs,mask,tick_label=names,align='center',color='white',edgecolor='black',hatch=patterns[i])

subplot(222)
title("10 processes")
ylabel("execution time in milliseconds(ms)")
#bar(locs,vals10,tick_label=names,align='center')
for i in range(6):
    mask=[0]*6
    mask[i]=vals10[i]
    bar(locs,mask,tick_label=names,align='center',color='white',edgecolor='black',hatch=patterns[i])

subplot(223)
title("25 processes")
ylabel("execution time in milliseconds(ms)")
#bar(locs,vals25,tick_label=names,align='center')
for i in range(6):
    mask=[0]*6
    mask[i]=vals25[i]
    bar(locs,mask,tick_label=names,align='center',color='white',edgecolor='black',hatch=patterns[i])

subplot(224)
title("50 processes")
ylabel("execution time in milliseconds(ms)")
#bar(locs,vals50,tick_label=names,align='center')
for i in range(6):
    mask=[0]*6
    mask[i]=vals50[i]
    bar(locs,mask,tick_label=names,align='center',color='white',edgecolor='black',hatch=patterns[i])

suptitle("iteration timings for reduced version and grid size 1000 on POWER8")





################################################
#exp 4



figure(figsize=figsize)

vals1=[2901,2764,4667,4617,5041,4707]
vals10=[2350,2305,4562,4073,4544,4095]
vals25=[2258,2349,4107,3983,4134,3996]
vals50=[2551,2341,4798,4318,4848,4365]



subplot(221)
title("1 process")
ylabel("execution time in milliseconds(ms)")
#bar(locs,vals1,tick_label=names,align='center')
for i in range(6):
    mask=[0]*6
    mask[i]=vals1[i]
    bar(locs,mask,tick_label=names,align='center',color='white',edgecolor='black',hatch=patterns[i])

subplot(222)
title("10 processes")
ylabel("execution time in milliseconds(ms)")
#bar(locs,vals10,tick_label=names,align='center')
for i in range(6):
    mask=[0]*6
    mask[i]=vals10[i]
    bar(locs,mask,tick_label=names,align='center',color='white',edgecolor='black',hatch=patterns[i])

subplot(223)
title("25 processes")
ylabel("execution time in milliseconds(ms)")
#bar(locs,vals25,tick_label=names,align='center')
for i in range(6):
    mask=[0]*6
    mask[i]=vals25[i]
    bar(locs,mask,tick_label=names,align='center',color='white',edgecolor='black',hatch=patterns[i])

subplot(224)
title("50 processes")
ylabel("execution time in milliseconds(ms)")
#bar(locs,vals50,tick_label=names,align='center')
for i in range(6):
    mask=[0]*6
    mask[i]=vals50[i]
    bar(locs,mask,tick_label=names,align='center',color='white',edgecolor='black',hatch=patterns[i])

suptitle("iteration timings for full version and grid size 1000 on POWER8")



























