#!/bin/bash

WORKDIR=${PWD}

#change appropriately, i.e. 2.8 or leave empty which refers to master
DUNEVERSION=2.7

FLAGS="-O3 -DNDEBUG -Wall"

DUNECOREMODULES="dune-common dune-istl dune-geometry dune-grid"

# build flags for all DUNE modules
# change according to your needs
if test -f config.opts ; then
  read -p "Found config.opts. Overwrite with default? (y,n) " YN
  if [ "$YN" != "y" ] ;then
    echo "Overwriting config.opts!"
    rm -f config.opts
  fi
fi

if ! test -f config.opts ; then
echo "\
DUNEPATH=`pwd`
BUILDDIR=build-cmake
USE_CMAKE=yes
MAKE_FLAGS=-j4
CMAKE_FLAGS=\"-DCMAKE_CXX_FLAGS=\\\"$FLAGS\\\"  \\
 -DCMAKE_POSITION_INDEPENDENT_CODE=TRUE \\
 -DDISABLE_DOCUMENTATION=TRUE \\
 -DCMAKE_DISABLE_FIND_PACKAGE_LATEX=TRUE\" " > config.opts
fi

DUNEBRANCH=
if [ "$DUNEVERSION" != "" ] ; then
  DUNEBRANCH="-b releases/$DUNEVERSION"
fi

# get all dune modules necessary
for MOD in $DUNECOREMODULES ; do
  git clone $DUNEBRANCH https://gitlab.dune-project.org/core/$MOD.git
done

# build all DUNE modules using dune-control
./dune-common/bin/dunecontrol --opts=config.opts all
