--------------------------------------------------------------------------
[[31807,1],41]: A high-performance Open MPI point-to-point messaging module
was unable to find any relevant network interfaces:

Module: OpenFabrics (openib)
  Host: power

Another transport will be used instead, although this may result in
lower performance.

NOTE: You can disable this warning by setting the MCA parameter
btl_base_warn_component_unused to 0.
--------------------------------------------------------------------------
Test all domain started with 50 processes and grid size 500
Jacobi converged in 280840 iterations
Gauss-Seidel RB converged in 160035 iterations
SOR RB converged in 90647 iterations

residual norm JAC is: 1.5999e-11
greatest wallclock time for JAC: 207.596
greatest wallclock time spent communicating for JAC: 24.6735
% of time spent communicating for JAC: 11.8853%
greatest wallclock time spent copying for JAC: 0
% of time spent copying for JAC: 0%

residual norm GS RB is: 8.03003e-12
greatest wallclock time for GS RB: 177.44
greatest wallclock time spent communicating for GS RB: 13.9375
% of time spent communicating for GS RB: 7.85481%
greatest wallclock time spent copying for GS RB: 0.403299
% of time spent copying for GS RB: 0.227288%

residual norm SOR RB is: 4.98547e-12
greatest wallclock time for SOR RB: 114.856
greatest wallclock time spent communicating for SOR RB: 13.1067
% of time spent communicating for SOR RB: 11.4114%
greatest wallclock time spent copying for SOR RB: 0.240011
% of time spent copying for SOR RB: 0.208966%
