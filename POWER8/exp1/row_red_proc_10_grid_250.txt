--------------------------------------------------------------------------
[[30995,1],7]: A high-performance Open MPI point-to-point messaging module
was unable to find any relevant network interfaces:

Module: OpenFabrics (openib)
  Host: power

Another transport will be used instead, although this may result in
lower performance.

NOTE: You can disable this warning by setting the MCA parameter
btl_base_warn_component_unused to 0.
--------------------------------------------------------------------------
Test all natural ordering started with 10 processes and grid size 250
Jacobi converged in 79212 iterations
Gauss-Seidel RB converged in 44783 iterations
SOR RB converged in 25223 iterations

residual norm JAC is: 1.59971e-11
greatest wallclock time for JAC: 17.6491
greatest wallclock time spent communicating for JAC: 2.1668
% of time spent communicating for JAC: 12.2771%
greatest wallclock time spent copying for JAC: 0
% of time spent copying for JAC: 0%

residual norm GS RB is: 8.03729e-12
greatest wallclock time for GS RB: 13.8412
greatest wallclock time spent communicating for GS RB: 1.21447
% of time spent communicating for GS RB: 8.77425%
greatest wallclock time spent copying for GS RB: 0.0380965
% of time spent copying for GS RB: 0.275239%

residual norm SOR RB is: 4.97154e-12
greatest wallclock time for SOR RB: 8.62602
greatest wallclock time spent communicating for SOR RB: 0.682101
% of time spent communicating for SOR RB: 7.90748%
greatest wallclock time spent copying for SOR RB: 0.0212295
% of time spent copying for SOR RB: 0.24611%
