--------------------------------------------------------------------------
[[25345,1],2]: A high-performance Open MPI point-to-point messaging module
was unable to find any relevant network interfaces:

Module: OpenFabrics (openib)
  Host: power

Another transport will be used instead, although this may result in
lower performance.

NOTE: You can disable this warning by setting the MCA parameter
btl_base_warn_component_unused to 0.
--------------------------------------------------------------------------
Test all domain started with 25 processes and grid size 250
Jacobi converged in 79339 iterations
Gauss-Seidel RB converged in 44848 iterations
SOR RB converged in 25256 iterations

residual norm JAC is: 1.59964e-11
greatest wallclock time for JAC: 19.8068
greatest wallclock time spent communicating for JAC: 3.38055
% of time spent communicating for JAC: 17.0676%
greatest wallclock time spent copying for JAC: 0
% of time spent copying for JAC: 0%

residual norm GS RB is: 8.03787e-12
greatest wallclock time for GS RB: 14.7041
greatest wallclock time spent communicating for GS RB: 1.86247
% of time spent communicating for GS RB: 12.6664%
greatest wallclock time spent copying for GS RB: 0.0467705
% of time spent copying for GS RB: 0.318078%

residual norm SOR RB is: 4.97419e-12
greatest wallclock time for SOR RB: 8.51282
greatest wallclock time spent communicating for SOR RB: 1.05355
% of time spent communicating for SOR RB: 12.3761%
greatest wallclock time spent copying for SOR RB: 0.0260425
% of time spent copying for SOR RB: 0.30592%
