--------------------------------------------------------------------------
[[32127,1],41]: A high-performance Open MPI point-to-point messaging module
was unable to find any relevant network interfaces:

Module: OpenFabrics (openib)
  Host: power

Another transport will be used instead, although this may result in
lower performance.

NOTE: You can disable this warning by setting the MCA parameter
btl_base_warn_component_unused to 0.
--------------------------------------------------------------------------
Test all domain started with 50 processes and grid size 100
Jacobi converged in 14740 iterations
Gauss-Seidel RB converged in 8613 iterations
SOR RB converged in 5006 iterations

residual norm JAC is: 1.59703e-11
greatest wallclock time for JAC: 1.68973
greatest wallclock time spent communicating for JAC: 0.771482
% of time spent communicating for JAC: 45.657%
greatest wallclock time spent copying for JAC: 0
% of time spent copying for JAC: 0%

residual norm GS RB is: 8.15303e-12
greatest wallclock time for GS RB: 1.12181
greatest wallclock time spent communicating for GS RB: 0.44664
% of time spent communicating for GS RB: 39.8141%
greatest wallclock time spent copying for GS RB: 0.00672133
% of time spent copying for GS RB: 0.599149%

residual norm SOR RB is: 4.87436e-12
greatest wallclock time for SOR RB: 0.663159
greatest wallclock time spent communicating for SOR RB: 0.261413
% of time spent communicating for SOR RB: 39.4194%
greatest wallclock time spent copying for SOR RB: 0.00381541
% of time spent copying for SOR RB: 0.575339%
