--------------------------------------------------------------------------
[[19452,1],41]: A high-performance Open MPI point-to-point messaging module
was unable to find any relevant network interfaces:

Module: OpenFabrics (openib)
  Host: power

Another transport will be used instead, although this may result in
lower performance.

NOTE: You can disable this warning by setting the MCA parameter
btl_base_warn_component_unused to 0.
--------------------------------------------------------------------------
Test all natural ordering started with 50 processes and grid size 100
Jacobi converged in 14739 iterations
Gauss-Seidel RB converged in 8087 iterations
SOR RB converged in 4430 iterations

residual norm JAC is: 1.59715e-11
greatest wallclock time for JAC: 1.72793
greatest wallclock time spent communicating for JAC: 0.694042
% of time spent communicating for JAC: 40.1661%
greatest wallclock time spent copying for JAC: 0
% of time spent copying for JAC: 0%

residual norm GS RB is: 7.98422e-12
greatest wallclock time for GS RB: 1.45658
greatest wallclock time spent communicating for GS RB: 0.7686
% of time spent communicating for GS RB: 52.7675%
greatest wallclock time spent copying for GS RB: 0
% of time spent copying for GS RB: 0%

residual norm SOR RB is: 4.98284e-12
greatest wallclock time for SOR RB: 0.802319
greatest wallclock time spent communicating for SOR RB: 0.416412
% of time spent communicating for SOR RB: 51.901%
greatest wallclock time spent copying for SOR RB: 0
% of time spent copying for SOR RB: 0%
