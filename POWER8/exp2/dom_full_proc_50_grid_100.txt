--------------------------------------------------------------------------
[[17929,1],38]: A high-performance Open MPI point-to-point messaging module
was unable to find any relevant network interfaces:

Module: OpenFabrics (openib)
  Host: power

Another transport will be used instead, although this may result in
lower performance.

NOTE: You can disable this warning by setting the MCA parameter
btl_base_warn_component_unused to 0.
--------------------------------------------------------------------------
Test all domain started with 50 processes and grid size 100
Jacobi converged in 14740 iterations
Gauss-Seidel RB converged in 8152 iterations
SOR RB converged in 4501 iterations

residual norm JAC is: 1.59703e-11
greatest wallclock time for JAC: 1.68938
greatest wallclock time spent communicating for JAC: 0.732447
% of time spent communicating for JAC: 43.3561%
greatest wallclock time spent copying for JAC: 0
% of time spent copying for JAC: 0%

residual norm GS RB is: 8.01162e-12
greatest wallclock time for GS RB: 1.43648
greatest wallclock time spent communicating for GS RB: 0.792101
% of time spent communicating for GS RB: 55.1418%
greatest wallclock time spent copying for GS RB: 0
% of time spent copying for GS RB: 0%

residual norm SOR RB is: 4.99818e-12
greatest wallclock time for SOR RB: 0.809719
greatest wallclock time spent communicating for SOR RB: 0.43274
% of time spent communicating for SOR RB: 53.4432%
greatest wallclock time spent copying for SOR RB: 0
% of time spent copying for SOR RB: 0%
