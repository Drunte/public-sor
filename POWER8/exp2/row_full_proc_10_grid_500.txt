--------------------------------------------------------------------------
[[18796,1],2]: A high-performance Open MPI point-to-point messaging module
was unable to find any relevant network interfaces:

Module: OpenFabrics (openib)
  Host: power

Another transport will be used instead, although this may result in
lower performance.

NOTE: You can disable this warning by setting the MCA parameter
btl_base_warn_component_unused to 0.
--------------------------------------------------------------------------
Test all natural ordering started with 10 processes and grid size 500
Jacobi converged in 280332 iterations
Gauss-Seidel RB converged in 157846 iterations
SOR RB converged in 88332 iterations

residual norm JAC is: 1.59991e-11
greatest wallclock time for JAC: 220.565
greatest wallclock time spent communicating for JAC: 12.0787
% of time spent communicating for JAC: 5.47625%
greatest wallclock time spent copying for JAC: 0
% of time spent copying for JAC: 0%

residual norm GS RB is: 8.00089e-12
greatest wallclock time for GS RB: 180.613
greatest wallclock time spent communicating for GS RB: 14.3163
% of time spent communicating for GS RB: 7.92649%
greatest wallclock time spent copying for GS RB: 0
% of time spent copying for GS RB: 0%

residual norm SOR RB is: 5.01409e-12
greatest wallclock time for SOR RB: 114.409
greatest wallclock time spent communicating for SOR RB: 7.98966
% of time spent communicating for SOR RB: 6.98341%
greatest wallclock time spent copying for SOR RB: 0
% of time spent copying for SOR RB: 0%
