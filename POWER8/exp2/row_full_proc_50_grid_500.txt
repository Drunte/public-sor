--------------------------------------------------------------------------
[[19115,1],42]: A high-performance Open MPI point-to-point messaging module
was unable to find any relevant network interfaces:

Module: OpenFabrics (openib)
  Host: power

Another transport will be used instead, although this may result in
lower performance.

NOTE: You can disable this warning by setting the MCA parameter
btl_base_warn_component_unused to 0.
--------------------------------------------------------------------------
Test all natural ordering started with 50 processes and grid size 500
Jacobi converged in 280810 iterations
Gauss-Seidel RB converged in 158036 iterations
SOR RB converged in 88396 iterations

residual norm JAC is: 1.59988e-11
greatest wallclock time for JAC: 238.001
greatest wallclock time spent communicating for JAC: 41.3411
% of time spent communicating for JAC: 17.3701%
greatest wallclock time spent copying for JAC: 0
% of time spent copying for JAC: 0%

residual norm GS RB is: 7.99905e-12
greatest wallclock time for GS RB: 217.158
greatest wallclock time spent communicating for GS RB: 64.7669
% of time spent communicating for GS RB: 29.8248%
greatest wallclock time spent copying for GS RB: 0
% of time spent copying for GS RB: 0%

residual norm SOR RB is: 5.01436e-12
greatest wallclock time for SOR RB: 125.473
greatest wallclock time spent communicating for SOR RB: 35.6373
% of time spent communicating for SOR RB: 28.4023%
greatest wallclock time spent copying for SOR RB: 0
% of time spent copying for SOR RB: 0%
