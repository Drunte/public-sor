--------------------------------------------------------------------------
[[18670,1],1]: A high-performance Open MPI point-to-point messaging module
was unable to find any relevant network interfaces:

Module: OpenFabrics (openib)
  Host: power

Another transport will be used instead, although this may result in
lower performance.

NOTE: You can disable this warning by setting the MCA parameter
btl_base_warn_component_unused to 0.
--------------------------------------------------------------------------
Test all natural ordering started with 25 processes and grid size 500
Jacobi converged in 280728 iterations
Gauss-Seidel RB converged in 158001 iterations
SOR RB converged in 88383 iterations

residual norm JAC is: 1.59991e-11
greatest wallclock time for JAC: 245.296
greatest wallclock time spent communicating for JAC: 25.4929
% of time spent communicating for JAC: 10.3927%
greatest wallclock time spent copying for JAC: 0
% of time spent copying for JAC: 0%

residual norm GS RB is: 7.99968e-12
greatest wallclock time for GS RB: 207.028
greatest wallclock time spent communicating for GS RB: 61.0957
% of time spent communicating for GS RB: 29.5109%
greatest wallclock time spent copying for GS RB: 0
% of time spent copying for GS RB: 0%

residual norm SOR RB is: 5.01417e-12
greatest wallclock time for SOR RB: 125.354
greatest wallclock time spent communicating for SOR RB: 34.2565
% of time spent communicating for SOR RB: 27.3279%
greatest wallclock time spent copying for SOR RB: 0
% of time spent copying for SOR RB: 0%
