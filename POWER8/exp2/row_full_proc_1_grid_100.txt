--------------------------------------------------------------------------
[[20358,1],0]: A high-performance Open MPI point-to-point messaging module
was unable to find any relevant network interfaces:

Module: OpenFabrics (openib)
  Host: power

Another transport will be used instead, although this may result in
lower performance.

NOTE: You can disable this warning by setting the MCA parameter
btl_base_warn_component_unused to 0.
--------------------------------------------------------------------------
Test all natural ordering started with 1 processes and grid size 100
Jacobi converged in 14740 iterations
Gauss-Seidel RB converged in 8087 iterations
SOR RB converged in 4429 iterations

residual norm JAC is: 1.59703e-11
greatest wallclock time for JAC: 4.21921
greatest wallclock time spent communicating for JAC: 0
% of time spent communicating for JAC: 0%
greatest wallclock time spent copying for JAC: 0
% of time spent copying for JAC: 0%

residual norm GS RB is: 7.97829e-12
greatest wallclock time for GS RB: 3.30014
greatest wallclock time spent communicating for GS RB: 0
% of time spent communicating for GS RB: 0%
greatest wallclock time spent copying for GS RB: 0
% of time spent copying for GS RB: 0%

residual norm SOR RB is: 4.98919e-12
greatest wallclock time for SOR RB: 2.00888
greatest wallclock time spent communicating for SOR RB: 0
% of time spent communicating for SOR RB: 0%
greatest wallclock time spent copying for SOR RB: 0
% of time spent copying for SOR RB: 0%
