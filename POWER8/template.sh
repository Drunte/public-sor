#!/bin/bash
prog=$1
fileprefix=$2
proc=$3
grid=$4
echo "test $proc processes, grid $grid started"
filename="$fileprefix$proc""_grid_$grid.txt"
>$filename
mpirun -np $proc $prog $grid >>$filename
echo "test $proc processes, grid $grid finished"
echo
