--------------------------------------------------------------------------
[[29467,1],6]: A high-performance Open MPI point-to-point messaging module
was unable to find any relevant network interfaces:

Module: OpenFabrics (openib)
  Host: power

Another transport will be used instead, although this may result in
lower performance.

NOTE: You can disable this warning by setting the MCA parameter
btl_base_warn_component_unused to 0.
--------------------------------------------------------------------------
Test all domain started with 25 processes and grid size 1000
Jacobi converged in 100 iterations
Gauss-Seidel RB converged in 100 iterations
SOR RB converged in 100 iterations

residual norm JAC is: 2.49756e-07
greatest wallclock time for JAC: 2.34908
greatest wallclock time spent communicating for JAC: 0.313085
% of time spent communicating for JAC: 13.328%
greatest wallclock time spent copying for JAC: 0
% of time spent copying for JAC: 0%

residual norm GS RB is: 4.98043e-07
greatest wallclock time for GS RB: 3.98276
greatest wallclock time spent communicating for GS RB: 1.11993
% of time spent communicating for GS RB: 28.1194%
greatest wallclock time spent copying for GS RB: 0
% of time spent copying for GS RB: 0%

residual norm SOR RB is: 1.22418e-06
greatest wallclock time for SOR RB: 3.99614
greatest wallclock time spent communicating for SOR RB: 1.20993
% of time spent communicating for SOR RB: 30.2774%
greatest wallclock time spent copying for SOR RB: 0
% of time spent copying for SOR RB: 0%
