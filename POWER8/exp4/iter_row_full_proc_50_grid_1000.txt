--------------------------------------------------------------------------
[[28781,1],43]: A high-performance Open MPI point-to-point messaging module
was unable to find any relevant network interfaces:

Module: OpenFabrics (openib)
  Host: power

Another transport will be used instead, although this may result in
lower performance.

NOTE: You can disable this warning by setting the MCA parameter
btl_base_warn_component_unused to 0.
--------------------------------------------------------------------------
Test all natural ordering started with 50 processes and grid size 1000
Jacobi converged in 100 iterations
Gauss-Seidel RB converged in 100 iterations
SOR RB converged in 100 iterations

residual norm JAC is: 2.49458e-07
greatest wallclock time for JAC: 2.55102
greatest wallclock time spent communicating for JAC: 0.141243
% of time spent communicating for JAC: 5.53674%
greatest wallclock time spent copying for JAC: 0
% of time spent copying for JAC: 0%

residual norm GS RB is: 4.98422e-07
greatest wallclock time for GS RB: 4.79804
greatest wallclock time spent communicating for GS RB: 1.84669
% of time spent communicating for GS RB: 38.4885%
greatest wallclock time spent copying for GS RB: 0
% of time spent copying for GS RB: 0%

residual norm SOR RB is: 1.22887e-06
greatest wallclock time for SOR RB: 4.84784
greatest wallclock time spent communicating for SOR RB: 1.62481
% of time spent communicating for SOR RB: 33.5162%
greatest wallclock time spent copying for SOR RB: 0
% of time spent copying for SOR RB: 0%
