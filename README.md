# public-SOR

install guide:

0) you will need cmake 3.10 or newer, a C compiler, a C++ compiler, a fortran compiler and MPI.

1) run the bash script build-dune-core.sh and say no to the prompt.

2) if all went well, the insatllation is now complete.


the source code can be found in dune-sor/src

the executables can be found in dune-sor/build-cmake/src

to run the executables, call: mpirun -np "number of processes" "executable" "positive int"
for example: mpirun -np 4 ./test_all_domain 10


test results: the folder POWER8 contains all the raw data results from my experiments.

