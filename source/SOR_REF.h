




#ifndef SOR_REF_H
#define SOR_REF_H

#include <vector>

using std::vector;

vector<double> SOR_REF(const vector<vector<double>> &A, const vector<double> &b, const vector<double> &x, double relaxation_factor, double tol);


#endif // !SOR_REF_H

















