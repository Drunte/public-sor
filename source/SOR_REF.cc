
#include <iostream>

#include "SOR_REF.h"
#include "lin_alg_util.h"

using std::vector;



vector<double> SOR_REF(const vector<vector<double>> &A, const vector<double> &b, const vector<double> &x, double relaxation_factor = 1.25, double tol = 1.e-6) {
	//placeholder types
	// relaxation factor should be in [1,2]
	vector<double> previous = x;
	vector<double> next = x;

	int iter = 0;
	do{
		iter++;
		for (size_t i = 0; i < x.size(); i++){
			previous[i] = next[i];
		}

		for (size_t row = 0; row < x.size(); row++){

			double sum = b[row];
			for (size_t col = 0; col < x.size(); col++){

				if (row != col){
					sum -= A[row][col] * next[col];
				}
			}

			next[row] += relaxation_factor * (sum / A[row][row] - next[row]);

		}


	} while (norm_2(sub(next,previous)) > tol);

	std::cout << "iterations: " << iter << std::endl;

	return next;
}












