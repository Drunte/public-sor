





#ifndef JAC_REF_H
#define JAC_REF_H

//#include "dune_precompiled_header.h"


#include <vector>

using std::vector;

vector<double> JAC_REF(const vector<vector<double>> &A, const vector<double> &b, const vector<double> &x, double tol);


#endif // !JAC_REF_H





