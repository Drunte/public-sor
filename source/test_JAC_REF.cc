




#include "JAC_REF.h"
#include "lin_alg_util.h"


#include <iostream>
using std::cout;
using std::endl;

int main() {

	//python ref solution: 0.16, 0.35, 0.44, 0.89
	cout << "Jacobi started\n" << endl;

	vector<vector<double>> A = { {4,1,0,0},
								 {1,4,1,0},
								 {0,1,4,1},
								 {0,0,1,4} };

	vector<double> b = {1,2,3,4};
	vector<double> x0 = { 0,0,0,0 };
	double tol = 1.e-6;
	auto x = JAC_REF(A, b, x0, tol);

	cout << "result:" << endl;

	for (const auto &xs:x) {
		cout << xs << ", ";
	}
	cout << endl;

	cout << "should be:\n0.16, 0.35, 0.44, 0.89" << endl;

	auto xsol = mat_vec(A, x);

	cout << "sanity check" << endl;

	for (const auto &xs : xsol) {
		cout << xs << ", ";
	}
	cout << endl;

	cout << "should be:\n";
	for (const auto &bs : b) {
		cout << bs << ", ";
	}
	cout << endl;

	return 0;
}





