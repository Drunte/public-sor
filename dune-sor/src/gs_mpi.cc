



#include <iostream>
#include <vector>

#include "gs_mpi.h"

#include "dune_pch.h"

#include "bundle.h"

using std::cout;
using std::endl;


typedef Dune::BCRSMatrix< double > Matrix;
typedef Dune::BlockVector< double > Vector;

typedef Dune::SimpleMessageBuffer MsgBuffer;
typedef Dune::Point2PointCommunicator< MsgBuffer > ptpComm;

template<class F>
void innerloop(const Matrix &A, const Vector &b, const Vector &readfrom, Vector &writeto, const F &skiprow) {
	
	auto endrow = A.end();
	for (auto row = A.begin(); row != endrow; ++row) {

		const size_t rowindex = row.index();
		if (skiprow(rowindex)) {
			continue;
		}
		double sum = b[rowindex];
		auto endcol = (*row).end();
		for (auto col = (*row).begin(); col != endcol; ++col) {

			const size_t colindex = col.index();
			if (rowindex != colindex) {

				sum -= (*col) * readfrom[colindex];
			}
		}

		writeto[rowindex] = sum / A[rowindex][rowindex];
	}
}

Vector gs_mpi(const Matrix &A, const Vector &b, const Vector &x, Bundle &bundle, double tol) {
	
	//b and x needs to have zeros corresponding to the ghostrows
	Vector previous(x.size()); //zeros
	Vector next(x);  //copy

	ptpComm& comm = bundle.comm;
	auto& ghostbools = bundle.ghostbools;
	
	//Vector res(x.size());
	const size_t half = A.N() / 2 + A.N() % 2;
	int iter = 0;
	double termination_criteria;
	auto redrow = [&half](const size_t row) {
		return row <= half;
	};

	auto blackrow = [&half](const size_t row) {
		return row > half;
	};
	do{
		iter++;
		/*
		if (iter == 100) {
			break;
		}
		*/
		//communikation stage
		bundle.sendAndRecieveUpdate(next);

		//swap
		next.swap(previous);

		//algorithm
		//first pass
		//innerloop                     skip
		innerloop(A, b, previous, next, blackrow);

		//extra communication step
		bundle.sendAndRecieveUpdate(next);

		 /*
		// copy instead
		if (comm.size() > 1) {
			const double timestart = bundle.timer.elapsed();
			for (size_t i : bundle.ghostrows) {
				next[i] = previous[i];
			}
			const double timeend = bundle.timer.elapsed();
			bundle.timeSpentCopying += timeend - timestart;
		}
		 */

		//second pass
		//innerloop                 skip
		innerloop(A, b, next, next, redrow);

		/*
		A.mv(next, res);
		res -= b;
		double partialsum = norm_square_with_ignore(res, ghostbools);
		//double partialsum = norm_of_diff_square(res, b, ghostbools);
		*/
		
		double partialsum = norm_of_diff_square(next, previous, ghostbools);
		//compute termination criteria
		termination_criteria = comm.sum(partialsum);

		//cout << termination_criteria << endl;

	} while (termination_criteria > tol*tol);

	bundle.sendAndRecieveUpdate(next);

	if (comm.rank()==0){
		cout << "Gauss-Seidel converged in " << iter << " iterations" << endl;
	}

	return next;
}



Vector gs_rb_mpi(const Matrix &A, const Vector &b, const Vector &x, Bundle &bundle, double tol) {
	//b and x needs to have zeros corresponding to the ghostrows
	Vector previous(x.size()); //zeros
	Vector next(x);  //copy

	ptpComm& comm = bundle.comm;
	auto& ghostbools = bundle.ghostbools;

	//Vector res(x.size());
	//const size_t half = A.N() / 2 + A.N() % 2;
	int iter = 0;
	double termination_criteria;

	/*
	auto redrow = [&half](const size_t row) {
		return row <= half;
	};

	auto blackrow = [&half](const size_t row) {
		return row > half;
	};
	*/

	auto redrow = [&bundle](const size_t row) { //this is only true for odd grids! need xgrid somehow
		return bundle.is_red(row);
	};

	auto blackrow  = [&bundle](const size_t row) {
		return bundle.is_black(row);
	};
	do {
		iter++;
		
		 /*
		if (iter == 100){
			break;
		}
		 */
		
		//communication stage
		bundle.sendAndRecieveUpdate(next);

		//swap
		next.swap(previous);

		//algorithm
		//first pass
		//innerloop                     skip
		innerloop(A, b, previous, next, blackrow);
		

		//extra communication step
		//bundle.sendAndRecieveUpdate(next);

		// /*
		//copy instead
		if (comm.size() > 1) {
			const double timestart = bundle.timer.elapsed();
			for (size_t i : bundle.ghostrows) {
				next[i] = previous[i];
			}
			const double timeend = bundle.timer.elapsed();
			bundle.timeSpentCopying += timeend - timestart;
		}
		// */

		//second pass
		//innerloop                 skip
		innerloop(A, b, next, next, redrow);
		

		/*
		A.mv(next, res);
		res -= b;
		double partialsum = norm_square_with_ignore(res, ghostbools);
		//double partialsum = norm_of_diff_square(res, b, ghostbools);
		*/

		double partialsum = norm_of_diff_square(next, previous, ghostbools);
		//compute termination criteria
		termination_criteria = comm.sum(partialsum);

		//cout << termination_criteria << endl;

	} while (termination_criteria > tol*tol);

	bundle.sendAndRecieveUpdate(next);

	if (comm.rank() == 0) {
		cout << "Gauss-Seidel RB converged in " << iter << " iterations" << endl;
	}

	return next;
}














