




#ifndef GS_MPI_H
#define GS_MPI_H

#include "dune_pch.h"

#include "bundle.h"


typedef Dune::BCRSMatrix< double > Matrix;
typedef Dune::BlockVector< double > Vector;

typedef Dune::SimpleMessageBuffer MsgBuffer;
typedef Dune::Point2PointCommunicator< MsgBuffer > ptpComm;


Vector gs_mpi(const Matrix &A, const Vector &b, const Vector &x, Bundle &bundle, double tol = 1.e-6);

Vector gs_rb_mpi(const Matrix &A, const Vector &b, const Vector &x, Bundle &bundle, double tol = 1.e-6);

#endif // !GS_MPI_H


















