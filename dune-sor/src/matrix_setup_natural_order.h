




#ifndef MAT_SETUP_N_O_H
#define MAT_SETUP_N_O_H

#include "dune_pch.h"
#include "rhs.h"

//#include <dune/common/fvector.hh>
//#include <dune/grid/yaspgrid.hh>

#include <cassert>
#include <array>
#include <set>
#include <map>
#include <vector>

void setupLinkageByRows(ptpComm &comm) {
	const size_t myrank = comm.rank();
	const size_t commsize = comm.size();
	const size_t lastrank = commsize - 1;
	const size_t firstrank = 0;

	std::set<int> sendlinks;
	std::set<int> recvlinks;

	if (commsize > 1) {
		if (myrank != lastrank) { //uplink
			sendlinks.insert(myrank + 1);
			recvlinks.insert(myrank + 1);
		}

		if (myrank != firstrank) { //downlink
			sendlinks.insert(myrank - 1);
			recvlinks.insert(myrank - 1);
		}
	}

	comm.insertRequest(sendlinks, recvlinks);
}


void setupGhostRows(ptpComm &comm, std::vector<size_t> &ghostrows, size_t xgrid, size_t dim) {
	const size_t myrank = comm.rank();
	const size_t commsize = comm.size();
	const size_t lastrank = commsize - 1;
	const size_t firstrank = 0;

	if (commsize > 1) {
		if (myrank != firstrank) {
			for (size_t i = 0; i < xgrid; i++) { //leading ghostrows
				ghostrows.emplace_back(i);
			}
		}

		if (myrank != lastrank) {
			for (size_t i = dim - xgrid; i < dim; i++) { //trailing ghostrows
				ghostrows.emplace_back(i);
			}
		}
	}
}

void setupSendRecvIndices(ptpComm &comm, std::vector<std::vector<size_t>> &sendindicesVec, std::vector<std::vector<size_t>> &recvindicesVec, size_t xgrid, size_t dim) {
	const size_t myrank = comm.rank();
	const size_t commsize = comm.size();
	const size_t lastrank = commsize - 1;
	const size_t firstrank = 0;

	if (commsize > 1) {
		std::vector<size_t> sendindices;
		std::vector<size_t> recvindices;

		if (myrank != lastrank) { //uplink

			for (size_t i = dim - 2 * xgrid; i < dim - xgrid; i++) {
				sendindices.emplace_back(i);
			}

			for (size_t i = dim - xgrid; i < dim; i++) {
				recvindices.emplace_back(i);
			}

			sendindicesVec.emplace_back(sendindices); //copy. is there a way to get around this copy?
			recvindicesVec.emplace_back(recvindices);
		}

		sendindices.clear();
		recvindices.clear();

		if (myrank != firstrank) { //downlink

			for (size_t i = xgrid; i < 2 * xgrid; i++) {
				sendindices.emplace_back(i);
			}

			for (size_t i = 0; i < xgrid; i++) {
				recvindices.emplace_back(i);
			}

			sendindicesVec.emplace_back(sendindices); //same for this copy.
			recvindicesVec.emplace_back(recvindices);
		}
	}
	else {
		//no communication for only 1 process
	}
}


void setupRHSbyRows(Vector& b, const size_t xgrid, const size_t ygrid, const size_t myrank, const size_t commsize ){

	const double dx = 1.0 / (xgrid); //xgrid is the grid width
	const double dy = 1.0 / (ygrid);
	const double dx2=dx/2;
	const double dy2=dy/2;
	const size_t offset = myrank * ygrid / commsize;
	const size_t compensator = myrank == 0 ? 0 : -1;
	
	const size_t dim = b.size();

	//target vector setup
	for (size_t i = 0; i < dim; i++) {

		const double x = (i % xgrid) * dx+dx2;
		const double y = (i / ygrid + offset + compensator) * dy+dy2;

		b[i] = dx * dy * rhs(x, y);
	}
	
}


void setupMatrixNaturalOrder(Matrix &A, const std::vector<bool> &ghostbools, size_t xgrid, size_t dim) {
	const double diag = 4.0;
	const double offdiag = -1.0;

	//matrix setup
	const size_t start = 0;
	const size_t end = start + dim;

	for (size_t i = start; i < end; i++) {
		if (ghostbools[i]) {
			A.entry(i, i) = diag;

			if (i + 1 < dim && (i + 1) % xgrid > 0) {
				A.entry(i, i + 1) = offdiag;
			}

			if (i + xgrid < dim) {
				A.entry(i, i + xgrid) = offdiag;
			}

			if (i > 0 && i % xgrid > 0) {
				A.entry(i, i - 1) = offdiag;
			}

			if (i >= xgrid) {
				A.entry(i, i - xgrid) = offdiag;
			}
		}
		else {
			A.entry(i, i) = 1.0;
		}
	}

	A.compress();
}





#endif // !MAT_SETUP_N_O_H


















