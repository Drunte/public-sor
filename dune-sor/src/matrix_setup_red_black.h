




#ifndef MAT_SETUP_R_B_H
#define MAT_SETUP_R_B_H

#include "dune_pch.h"

#include <set>




//use local indexing. maps into [0,dim-1]   0->0 1->something 2->1 etc
size_t mapNaturalToRedBlackIndex(size_t i, size_t xgrid, size_t dim) {
	//for computing location of ghostrows
	size_t out = dim; //illegal value, used for inducing crashes / debug info
	if (xgrid % 2 == 1) { //odd gridsize

		if (i % 2 == 0) { //even natural index are red.
			out = i / 2;
		}
		else {
			out = i / 2 + dim / 2 + dim % 2;
		}
	}
	else { //even grid size
		size_t gridrow = i / xgrid; 

		if (gridrow % 2 == 0){ //even row
			if (i % 2 == 0){  //even index
				out = i / 2;
			}
			else{
				out = i / 2 + dim / 2;
			}
		}
		else{ //odd row
			if (i % 2 == 0){ //even index
				out = i / 2 + dim / 2;
			}
			else{
				out = i / 2;
			}
		}
	}
	return out;
}




void setupLinkageByRows(ptpComm &comm) {
	const size_t myrank = comm.rank();
	const size_t commsize = comm.size();
	const size_t lastrank = commsize - 1;
	const size_t firstrank = 0;

	std::set<int> sendlinks;
	std::set<int> recvlinks;

	if (commsize > 1) {
		if (myrank != lastrank) { //uplink
			sendlinks.insert(myrank + 1);
			recvlinks.insert(myrank + 1);
		}

		if (myrank != firstrank) { //downlink
			sendlinks.insert(myrank - 1);
			recvlinks.insert(myrank - 1);
		}
	}

	comm.insertRequest(sendlinks, recvlinks);
}

void setupLinkageByBlocks(ptpComm &comm) {
	//red-black partitioning
}


void setupGhostRowsRB(ptpComm &comm, std::vector<size_t> &ghostrows, size_t xgrid, size_t dim) {
	const size_t myrank = comm.rank();
	const size_t commsize = comm.size();
	const size_t lastrank = commsize - 1;
	const size_t firstrank = 0;

	if (commsize > 1) {
		if (myrank != firstrank) {
			for (size_t i = 0; i < xgrid; i++) { //leading ghostrows
				ghostrows.emplace_back(mapNaturalToRedBlackIndex(i, xgrid, dim));
			}
		}

		if (myrank != lastrank) {
			for (size_t i = dim - xgrid; i < dim; i++) { //trailing ghostrows
				ghostrows.emplace_back(mapNaturalToRedBlackIndex(i, xgrid, dim));
			}
		}
	}
}

void setupSendRecvIndecisRB(ptpComm &comm, std::vector<std::vector<size_t>> &sendindicesVec, std::vector<std::vector<size_t>> &recvindicesVec, size_t xgrid, size_t dim) {
	const size_t myrank = comm.rank();
	const size_t commsize = comm.size();
	const size_t lastrank = commsize - 1;
	const size_t firstrank = 0;

	if (commsize > 1) {
		std::vector<size_t> sendindices;
		std::vector<size_t> recvindices;

		if (myrank != lastrank) { //uplink

			for (size_t i = dim - 2 * xgrid; i < dim - xgrid; i++) {
				sendindices.emplace_back(mapNaturalToRedBlackIndex(i, xgrid, dim));
			}

			for (size_t i = dim - xgrid; i < dim; i++) {
				recvindices.emplace_back(mapNaturalToRedBlackIndex(i, xgrid, dim));
			}

			sendindicesVec.emplace_back(sendindices); //copy. is there a way to get around this copy?
			recvindicesVec.emplace_back(recvindices);
		}

		sendindices.clear();
		recvindices.clear();

		if (myrank != firstrank) { //downlink

			for (size_t i = xgrid; i < 2 * xgrid; i++) {
				sendindices.emplace_back(mapNaturalToRedBlackIndex(i, xgrid, dim));
			}

			for (size_t i = 0; i < xgrid; i++) {
				recvindices.emplace_back(mapNaturalToRedBlackIndex(i, xgrid, dim));
			}

			sendindicesVec.emplace_back(sendindices); //same for this copy.
			recvindicesVec.emplace_back(recvindices);
		}
	}
	else {
		//no communication for only 1 process
	}
}



void setupMatrixRedBlack(Matrix &A, const std::vector<bool> &ghostbools, size_t xgrid, size_t dim) {
	const double diag = 4.0;
	const double offdiag = -1.0;

	for (size_t i = 0; i < dim; i++) {
		size_t mapped_i = mapNaturalToRedBlackIndex(i, xgrid, dim);
		if (ghostbools[mapped_i]) {
			
			A.entry(mapped_i, mapped_i) = diag;

			if (i + 1 < dim && (i + 1) % xgrid > 0) {
				A.entry(mapped_i, mapNaturalToRedBlackIndex(i + 1, xgrid, dim)) = offdiag;
			}

			if (i + xgrid < dim) {
				A.entry(mapped_i, mapNaturalToRedBlackIndex(i + xgrid, xgrid, dim)) = offdiag;
			}

			if (i > 0 && i % xgrid > 0) {
				A.entry(mapped_i, mapNaturalToRedBlackIndex(i - 1, xgrid, dim)) = offdiag;
			}

			if (i >= xgrid) {
				A.entry(mapped_i, mapNaturalToRedBlackIndex(i - xgrid, xgrid, dim)) = offdiag;
			}
		}
		else {
			A.entry(mapped_i, mapped_i) = 1.0;
		}
	}

	A.compress();
}

#endif // !MAT_SETUP_R_B_H


















