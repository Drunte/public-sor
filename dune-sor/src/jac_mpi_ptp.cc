



#include <iostream>
#include <vector>

#include "jac_mpi_ptp.h"

#include "dune_pch.h"

#include "bundle.h"

using std::cout;
using std::endl;


typedef Dune::BCRSMatrix< double > Matrix;
typedef Dune::BlockVector< double > Vector;

typedef Dune::SimpleMessageBuffer MsgBuffer;
typedef Dune::Point2PointCommunicator< MsgBuffer > ptpComm;



Vector jac_mpi_ptp(const Matrix &A, const Vector &b, const Vector &x, Bundle &bundle, double tol) {

	//todo: bundle class p2pcomm,ghostrow vector,send and recv vectors,bool vector... done
	
	//b and x needs to have zeros corresponding to the ghostrows
	Vector previous(x.size()); //zeros
	Vector next(x);  //copy

	ptpComm& comm = bundle.comm;
	auto& ghostbools = bundle.ghostbools;
	
	int iter = 0;
	double termination_criteria;
	do{
		iter++;
		 /*
		if (iter == 100){
			break;
		}
		 */
		//communikation stage
		bundle.sendAndRecieveUpdate(next);

		//swap roles
		next.swap(previous);

		//algorithm
		auto endrow = A.end();
		for (auto row = A.begin(); row != endrow; ++row) {

			const size_t rowindex = row.index();
			double sum = b[rowindex];
			auto endcol = (*row).end();
			for (auto col = (*row).begin(); col != endcol; ++col) {

				const size_t colindex = col.index();
				if (rowindex != colindex) { 
									
					sum -= (*col) * previous[colindex]; 
				}
			}

			next[rowindex] = sum / A[rowindex][rowindex]; 
		}
		
		double partialsum = norm_of_diff_square(next, previous, ghostbools);
		//compute termination criteria
		termination_criteria = comm.sum(partialsum);

		//cout << termination_criteria << endl;

	} while (termination_criteria > tol*tol);

	bundle.sendAndRecieveUpdate(next);

	if (comm.rank()==0){
		cout << "Jacobi converged in " << iter << " iterations" << endl;
	}

	return next;
}














