




#ifndef SOR_MPI_H
#define SOR_MPI_H

#include "dune_pch.h"

#include "bundle.h"


typedef Dune::BCRSMatrix< double > Matrix;
typedef Dune::BlockVector< double > Vector;

typedef Dune::SimpleMessageBuffer MsgBuffer;
typedef Dune::Point2PointCommunicator< MsgBuffer > ptpComm;


Vector sor_mpi(const Matrix &A, const Vector &b, const Vector &x, Bundle &bundle, double relaxation_factor = 1.28, double tol = 1.e-6);

Vector sor_rb_mpi(const Matrix &A, const Vector &b, const Vector &x, Bundle &bundle, double relaxation_factor = 1.28, double tol = 1.e-6);

#endif // !SOR_MPI_H


















