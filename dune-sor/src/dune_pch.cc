



#include <cmath>
#include <iostream>

//trying to make an object file
#include "dune_pch.h"

using std::cout;
using std::endl;

typedef Dune::BlockVector< double > Vector;


double norm_square_with_ignore(const Vector &x, const std::vector<bool> &ghostbools) {
	double sum = 0.0;
	for (size_t i = 0; i < x.size(); i++){
		if (ghostbools[i]){
			double val = x[i];
			sum += val*val;

		}

	}
	return sum;
}




double norm_of_diff_square(ptpComm &comm, const Vector &left, const Vector &right) {

	double sum = 0;
	double diff;
	size_t myrank = comm.rank();
	size_t col_size = left.size(); //the shorter of the two vectors
	size_t offset = myrank * col_size;

	for (size_t i = 0; i < left.size(); i++){

		diff = left[i] - right[i + offset];
		sum += diff * diff;
	}

	//return comm.sum(sum);
	return sum;
}







double norm_of_diff_square(const Vector &left, const Vector &right) {
	

	double sum = 0.0;
	double diff;
	for (size_t i = 0; i < left.size(); i++){
		diff = left[i] - right[i];
		sum += diff * diff;
	}
	return sum;
}


//todo add ghostrow vector to skip
double norm_of_diff_square(const Vector &left, const Vector &right, const std::vector<bool> &ghostbools) {

	double sum = 0.0;
	double diff;
	for (size_t i = 0; i < left.size(); i++){

		if (ghostbools[i]){ //false for ghostrows, true for real rows
			diff = left[i] - right[i];
			sum += diff * diff;

		}
	}

	return sum;
}


double norm_of_diff_square(const Vector &left, const Vector &right, const size_t start, const size_t end) {

	double sum = 0.0;
	double diff;
	for (size_t i = start; i < end; i++){

		diff = left[i] - right[i];
		sum += diff * diff;
	}

	return sum;
}







double norm_of_diff(const Vector &left, const Vector &right) {
	using std::sqrt;
	return sqrt(norm_of_diff_square(left, right));
}



Vector matvec(const Matrix &A, const Vector &x, ptpComm &comm) {
	const size_t rows = A.N();
	Vector out(rows);

	auto endrow = A.end();
	for (auto row = A.begin(); row != endrow; ++row) {

		double sum = 0.0;
		auto endcol = (*row).end();
		for (auto col = (*row).begin(); col != endcol; ++col) {
			sum += (*col) * x[col.index()];
		}

		out[row.index()] = sum;
	}

	return out;
}




Vector matvec_collect(const Matrix &A, const Vector &x, ptpComm &comm) {

	Vector out(A.M());
	Vector part = matvec(A, x, comm);
	comm.allgather(part.data(), part.size(), out.data());
	return out;
}




void print(const Vector &V) {

	for (size_t i = 0; i < V.size(); i++){
		cout << V[i] << ", ";
	}
	cout << endl;
}





void print(const Matrix &A) {

	auto endrow = A.end();
	for (auto row = A.begin(); row != endrow; ++row) {
		auto endcol = (*row).end();
		for (auto col = (*row).begin(); col != endcol; ++col) {
			cout << "row: " << row.index() << " col: " << col.index() << " value: " << *col << ", ";
		}
		cout << endl;
	}
}






