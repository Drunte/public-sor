




#ifndef JAC_MPI_PTP_H
#define JAC_MPI_PTP_H

#include "dune_pch.h"

#include "bundle.h"


typedef Dune::BCRSMatrix< double > Matrix;
typedef Dune::BlockVector< double > Vector;

typedef Dune::SimpleMessageBuffer MsgBuffer;
typedef Dune::Point2PointCommunicator< MsgBuffer > ptpComm;


Vector jac_mpi_ptp(const Matrix &A, const Vector &b, const Vector &x, Bundle &bundle, double tol = 1.e-6);



#endif // !JAC_MPI_PTP_H


















