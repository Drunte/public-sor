#add_executable("dune-sor" dune-sor.cc)
#target_link_dune_default_libraries("dune-sor")

#add_executable(dune-pch dune_pch.cc)
#target_link_dune_default_libraries("dune-pch")

#add_executable(test_gs_ref_std test_gs_ref_std.cc gs_ref_std.cc lin_alg_util.cc)
#target_link_dune_default_libraries("test_gs_ref_std")
#add_executable(test_jac_ref_std test_jac_ref_std.cc jac_ref_std.cc lin_alg_util.cc)
#target_link_dune_default_libraries("test_jac_ref_std")
#add_executable(test_sor_ref_std test_sor_ref_std.cc sor_ref_std.cc lin_alg_util.cc)
#target_link_dune_default_libraries("test_sor_ref_std")

#add_executable(test_jac_ref_bcrs test_jac_ref_bcrs.cc jac_ref_bcrs.cc dune_pch.cc)
#target_link_dune_default_libraries("test_jac_ref_bcrs")
#add_executable(test_gs_ref_bcrs test_gs_ref_bcrs.cc gs_ref_bcrs.cc dune_pch.cc)
#target_link_dune_default_libraries("test_gs_ref_bcrs")
#add_executable(test_sor_ref_bcrs test_sor_ref_bcrs.cc sor_ref_bcrs.cc dune_pch.cc)
#target_link_dune_default_libraries("test_sor_ref_bcrs")


#add_executable(test_jac_mpi_ptp test_jac_mpi_ptp.cc jac_mpi_ptp.cc dune_pch.cc bundle.cc)
#target_link_dune_default_libraries("test_jac_mpi_ptp")
#add_executable(test_gs_mpi test_gs_mpi.cc gs_mpi.cc jac_mpi_ptp.cc dune_pch.cc bundle.cc)
#target_link_dune_default_libraries("test_gs_mpi")
#add_executable(test_sor_mpi test_sor_mpi.cc sor_mpi.cc gs_mpi.cc jac_mpi_ptp.cc dune_pch.cc bundle.cc)
#target_link_dune_default_libraries("test_sor_mpi")
add_executable(test_all_natural_order test_all_natural_order.cc jac_mpi_ptp.cc gs_mpi.cc sor_mpi.cc dune_pch.cc bundle.cc)
target_link_dune_default_libraries("test_all_natural_order")
add_executable(test_all_domain test_all_domain.cc jac_mpi_ptp.cc gs_mpi.cc sor_mpi.cc dune_pch.cc bundle.cc)
target_link_dune_default_libraries("test_all_domain")


#add_executable(test_red_black_mapping test_red_black_mapping.cc)
#target_link_dune_default_libraries("test_red_black_mapping")

#add_executable(test_jac_mpi_all test_jac_mpi_all.cc jac_mpi_all.cc dune_pch.cc)
#target_link_dune_default_libraries("test_jac_mpi_all")

#add_executable(test_jac_2p_ptp test_jac_2p_ptp.cc jac_mpi_ptp.cc dune_pch.cc bundle.cc)
#target_link_dune_default_libraries("test_jac_2p_ptp")

#add_executable(device_info device.cu)
#target_link_dune_default_libraries("device_info")





