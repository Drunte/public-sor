





#include "bundle.h"

#include "dune_pch.h"




void Bundle::sendAndRecieveUpdate(Vector &next) {
	
	if (comm.size() == 1) return;

	const double commstart = timer.elapsed();

	const size_t sendlinks = comm.sendLinks();
	std::vector<MsgBuffer> sendbuffers(sendlinks);

	for (size_t i = 0; i < sendlinks; i++) {
		for (const auto& sendindex : sendindicesVec[i]) {

			double val = next[sendindex];
			sendbuffers[i].write(val);
		}
	}

	std::vector<MsgBuffer> recvbuffers = comm.exchange(sendbuffers);
	const size_t recvlinks = comm.recvLinks();

	for (size_t i = 0; i < recvlinks; i++) {

		for (const auto& recvindex : recvindicesVec[i]) {

			double val;
			recvbuffers[i].read(val);
			next[recvindex] = val;
		}
	}

	const double commend = timer.elapsed();
	timeSpentCommunicating += commend - commstart;
}

bool Bundle::is_black(const size_t row) {
	if (gridsize % 2 != 0 || (row / gridsize) % 2 == 0) {
		return row % 2 != 0;
	}
	else{
		//more complicated
		return row % 2 == 0;
	}
}


bool Bundle::is_red(const size_t row) {
	if (gridsize % 2 != 0 || (row / gridsize) % 2 == 0) {
		return row % 2 == 0;
	}
	else{
		//more complicated
		return row % 2 != 0;
	}
}

