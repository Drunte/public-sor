







#ifndef BUNDLE_H
#define BUNDLE_H

#include "dune_pch.h"

#include <vector>



class Bundle {

public: // all member states (aka attributes / fields / etc)
	ptpComm& comm;

	Dune::Timer& timer;

	const size_t gridsize;

	const std::vector<size_t>& ghostrows;
	const std::vector<bool>& ghostbools;

	const std::vector<std::vector<size_t>>& sendindicesVec;
	const std::vector<std::vector<size_t>>& recvindicesVec;

	double timeSpentCommunicating = 0.0;
	double timeSpentCopying = 0.0;

public: // all constructors
	Bundle() = delete; //no default constructor

	Bundle(ptpComm& c, Dune::Timer& t, const size_t &xgrid, std::vector<size_t>& gr, std::vector<bool>& gb, std::vector<std::vector<size_t>>& s, std::vector<std::vector<size_t>>& r) :
		comm(c), timer(t), gridsize(xgrid), ghostrows(gr), ghostbools(gb), sendindicesVec(s), recvindicesVec(r) {}; //are these copy statements?

public: // all member functions (aka methods)

	void sendAndRecieveUpdate(Vector &next);

	bool is_red(const size_t row);

	bool is_black(const size_t row);
};




#endif // !BUNDLE_H




















