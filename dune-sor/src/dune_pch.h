



#ifndef DUNE_PRECOMPILED_HEADER_H
#define DUNE_PRECOMPILED_HEADER_H

#ifdef HAVE_CONFIG_H
# include "config.h"
#endif


//this is an attemt at making a pre-compiled header( eg. library) 
#include <dune/common/exceptions.hh>
#include <dune/istl/bcrsmatrix.hh>
#include <dune/istl/bvector.hh>
#include <dune/common/parallel/mpihelper.hh>

#include <dune/common/timer.hh>

#include "comm/p2pcommunicator.hh"

#include <vector>

typedef Dune::BlockVector< double > Vector;
typedef Dune::BCRSMatrix< double > Matrix;

typedef Dune::SimpleMessageBuffer MsgBuffer;
typedef Dune::Point2PointCommunicator< MsgBuffer > ptpComm;



double norm_of_diff(const Vector &left,const Vector &right);

double norm_of_diff_square(const Vector &left, const Vector &right);

double norm_of_diff_square(const Vector &left, const Vector &right, const std::vector<bool> &ghostbools);

double norm_of_diff_square(const Vector &left, const Vector &right, const size_t start, const size_t end);

double norm_of_diff_square(ptpComm &comm, const Vector &left, const Vector &right);

void print(const Vector &V);

void print(const Matrix &A);

Vector matvec(const Matrix &A, const Vector &x, ptpComm &comm);

Vector matvec_collect(const Matrix &A, const Vector &x, ptpComm &comm);

double norm_square_with_ignore(const Vector &x, const std::vector<bool> &ghostbools);



#endif // !DUNE_PRECOMPILED_HEADER_H


