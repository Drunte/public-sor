




#include "jac_mpi_ptp.h"
#include "gs_mpi.h"
#include "sor_mpi.h"

#include "dune_pch.h"

#include "bundle.h"

#include "matrix_setup_natural_order.h"

#include <chrono>
#include <thread>



#include <vector>
//#include <set>
#include <string>
#include <iostream>
using std::cout;
using std::endl;
using std::stoi;
//using std::set;




void printmsg(const std::string& name, const double resnorm, const double maxtime, const double maxcomm, const double maxcopy) {
	cout << endl;
	cout << "residual norm " << name << " is: " << resnorm << endl;
	cout << "greatest wallclock time for " << name << ": " << maxtime << endl;
	cout << "greatest wallclock time spent communicating for " << name << ": " << maxcomm << endl;
	cout << "% of time spent communicating for " << name << ": " << 100 * maxcomm / maxtime << "%" << endl;
	cout << "greatest wallclock time spent copying for " << name << ": " << maxcopy << endl;
	cout << "% of time spent copying for " << name << ": " << 100 * maxcopy / maxtime << "%" << endl;

}









int main(int argc, char** argv) {

	Dune::MPIHelper::instance(argc, argv);

	if (argc != 2){
		cout << "missing commandline argument: need exactly 1 unsigned int >= 2, representing the gridsize" << endl;
		cout << "run with: mpirun -np <process_count> ./test_all_natural_order <gridsize>" << endl;
		exit(1);
	}

	ptpComm comm;
	const size_t myrank = comm.rank();
	const size_t commsize = comm.size();
	const size_t lastrank = commsize - 1;
	const size_t firstrank = 0;
	
	const int input = stoi(argv[1]);

	if (myrank == firstrank) {
		cout << "Test all natural ordering started with " << commsize << " processes and grid size "<< input << endl;
	}

	

	if (input < 2) {
		cout << "input must be positive and >= 2. aborting... " << endl;
		exit(1);
	}

	if (input % commsize != 0) {
		if (myrank == firstrank) {
			cout << "process number: " << commsize << " does not divide gridsize: " << input << " evenly! aborting... " << endl;
		}
		exit(2);
	}

	const size_t xgrid = input;
	const size_t ygrid = input;
	const size_t cols = xgrid * ygrid;
	const size_t realrows = cols / commsize; 
	const size_t ghostrowsnr = commsize == 1 ? 0 : (myrank == firstrank || myrank == lastrank) ? xgrid : 2 * xgrid; //amount of ghostrows
	
	const size_t dim = realrows + ghostrowsnr;

	
	//setup linkage
	setupLinkageByRows(comm);

	//ghostrows setup
	std::vector<size_t> ghostrows;
	setupGhostRows(comm, ghostrows, xgrid, dim);

	std::vector<bool> ghostbools(dim, true);
	for (auto r : ghostrows) {		
		ghostbools[r] = false;
	}

	//send and recv links
	std::vector<std::vector<size_t>> sendindicesVec;
	std::vector<std::vector<size_t>> recvindicesVec;
	setupSendRecvIndices(comm, sendindicesVec, recvindicesVec, xgrid, dim);
	

	//bundle setup
	Dune::Timer timer(false);
	Bundle bundle(comm, timer, xgrid, ghostrows, ghostbools, sendindicesVec, recvindicesVec);


	Matrix A(dim, dim, 5, 1.0, Matrix::implicit); 
	setupMatrixNaturalOrder(A, ghostbools, xgrid, dim);
	

	//initial guess setup
	Vector initialguess(dim);
	


	
	//target vector setup
	Vector b(dim);
	setupRHSbyRows(b, xgrid, ygrid, myrank, commsize);
	
	

	//call algorithm
	double relax = 1.33;
	timer.start();
	auto x1 = jac_mpi_ptp(A, b, initialguess, bundle);
	double timeelapsed1 = timer.elapsed();

	double timecommjac = bundle.timeSpentCommunicating;
	bundle.timeSpentCommunicating = 0.0;
	double timecopyjac = bundle.timeSpentCopying;
	bundle.timeSpentCopying = 0.0;

	timer.reset();
	auto x2 = gs_rb_mpi(A, b, initialguess, bundle);
	double timeelapsed2 = timer.elapsed();

	double timecommgs = bundle.timeSpentCommunicating;
	bundle.timeSpentCommunicating = 0.0;
	double timecopygs = bundle.timeSpentCopying;
	bundle.timeSpentCopying = 0.0;

	timer.reset();
	auto x3 = sor_rb_mpi(A, b, initialguess, bundle, relax);
	double timeelapsed3 = timer.elapsed();

	double timecommsor = bundle.timeSpentCommunicating;
	bundle.timeSpentCommunicating = 0.0;
	double timecopysor = bundle.timeSpentCopying;
	bundle.timeSpentCopying = 0.0;

	Vector y1(dim);
	Vector y2(dim);
	Vector y3(dim);
	//sanity check
	A.mv(x1, y1);
	A.mv(x2, y2);
	A.mv(x3, y3);


	double partial1 = norm_of_diff_square(b, y1, ghostbools); // all b y and x should have zeros corresponding to the ghost rows
	double resnorm1 = comm.sum(partial1);

	double partial2 = norm_of_diff_square(b, y2, ghostbools);
	double resnorm2 = comm.sum(partial2);

	double partial3 = norm_of_diff_square(b, y3, ghostbools);
	double resnorm3 = comm.sum(partial3);

	//std::this_thread::sleep_for(std::chrono::milliseconds(30));
	double delta = timeelapsed2;
	double delta2 = timeelapsed3;
	double maxtime = comm.max(timeelapsed1);
	double max_gs = comm.max(delta);
	double max_sor = comm.max(delta2);
	double max_comm_jac = comm.max(timecommjac);
	double max_comm_gs = comm.max(timecommgs);
	double max_comm_sor = comm.max(timecommsor);
	double max_copy_jac = comm.max(timecopyjac);
	double max_copy_gs = comm.max(timecopygs);
	double max_copy_sor = comm.max(timecopysor);
	if (myrank == firstrank) {
		printmsg("JAC", resnorm1, maxtime, max_comm_jac, max_copy_jac);
		printmsg("GS RB", resnorm2, max_gs, max_comm_gs, max_copy_gs);
		printmsg("SOR RB", resnorm3, max_sor, max_comm_sor, max_copy_sor);
	}

	return 0;
}





















