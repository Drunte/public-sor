#include <iostream>
#include <vector>
#include "sor_mpi.h"
#include "dune_pch.h"
#include "bundle.h"
using std::cout;
using std::endl;
typedef Dune::BCRSMatrix< double > Matrix;
typedef Dune::BlockVector< double > Vector;
typedef Dune::SimpleMessageBuffer MsgBuffer;
typedef Dune::Point2PointCommunicator< MsgBuffer > ptpComm;

Vector sor_rb_mpi(const Matrix &A, const Vector &b, const Vector &x, Bundle &bundle, double relaxation_factor, double tol) {
	Vector previous(x.size()); //zeros
	Vector next(x);  //copy of initial guess. we need 2 buffers for the Cauchy termination criteria.

	ptpComm& comm = bundle.comm; //communicator
	auto& ghostbools = bundle.ghostbools; //vector<bool> with False for ghostrows

	int iter = 0; //iteration counter
	double termination_criteria; 
	do {
		iter++; //increase iteration counter

		//communication stage. send and recieve the most recently updated values. 
		bundle.sendAndRecieveUpdate(next);

		//swap values such that previous has the values from the previous iteration.
		next.swap(previous);

		//algorithm
		//first pass, update red values.
		auto endrow = A.end();
		for (auto row = A.begin(); row != endrow; ++row) {
			const size_t rowindex = row.index();
			if (bundle.is_black(rowindex)){  //skip updating black values.
				continue;
			}
			double sum = b[rowindex]; //starting with b and subtracting later saves 1 flop.
			auto endcol = (*row).end();
			for (auto col = (*row).begin(); col != endcol; ++col) {
				const size_t colindex = col.index();
				if (rowindex != colindex) {
					sum -= (*col) * previous[colindex];
				}
			}
			//update scheme for SOR.
			next[rowindex] = previous[rowindex] + relaxation_factor * (sum / A[rowindex][rowindex] - previous[rowindex]);
		}

		//extra communication step. send and recieve red values.
		bundle.sendAndRecieveUpdate(next);
		//second pass. update black values.
		for (auto row = A.begin(); row != endrow; ++row) {
			const size_t rowindex = row.index();
			if (bundle.is_red(rowindex)) { //skip the red values this time.
				continue;
			}
			double sum = b[rowindex];
			auto endcol = (*row).end();
			for (auto col = (*row).begin(); col != endcol; ++col) {
				const size_t colindex = col.index();
				if (rowindex != colindex) {
					sum -= (*col) * next[colindex];
				}
			}
			next[rowindex] = previous[rowindex] + relaxation_factor * (sum / A[rowindex][rowindex] - previous[rowindex]);
		}
		
		double partialsum = norm_of_diff_square(next, previous, ghostbools); //compute the 2-norm squared of the difference, ignore ghost-values.
		//compute termination criteria
		termination_criteria = comm.sum(partialsum);  //sum all partial sums.
	} while (termination_criteria > tol*tol); //2-norm < tol is equvalent to 2-norm squared < tol squared.

	bundle.sendAndRecieveUpdate(next);  //do the final update

	if (comm.rank() == 0) {
		cout << "SOR RB converged in " << iter << " iterations" << endl;  //print info
	}
	return next;  //return the final iterate
}
