



#include <iostream>
#include <vector>

#include "sor_mpi.h"

#include "dune_pch.h"

#include "bundle.h"

using std::cout;
using std::endl;


typedef Dune::BCRSMatrix< double > Matrix;
typedef Dune::BlockVector< double > Vector;

typedef Dune::SimpleMessageBuffer MsgBuffer;
typedef Dune::Point2PointCommunicator< MsgBuffer > ptpComm;



Vector sor_mpi(const Matrix &A, const Vector &b, const Vector &x, Bundle &bundle, double relaxation_factor, double tol) {
	
	//b and x needs to have zeros corresponding to the ghostrows
	Vector previous(x.size()); //zeros
	Vector next(x);  //copy

	ptpComm& comm = bundle.comm;
	auto& ghostbools = bundle.ghostbools;
	
	//const size_t half = ceil((double)A.N() / 2.0); //if N is odd, there is 1 more red than black
	const size_t half = A.N() / 2 + A.N() % 2;
	int iter = 0;
	double termination_criteria;
	do{
		iter++;
		/*
		if (iter == 100) {
			break;
		}
		*/
		//communikation stage
		bundle.sendAndRecieveUpdate(next);

		//swap
		next.swap(previous);

		//algorithm
		//first pass
		auto endrow = A.end();
		for (auto row = A.begin(); row != endrow; ++row) {

			const size_t rowindex = row.index();
			if (rowindex > half) {
				continue;
			}
			double sum = b[rowindex];
			auto endcol = (*row).end();
			for (auto col = (*row).begin(); col != endcol; ++col) {

				const size_t colindex = col.index();
				if (rowindex != colindex) { 
									
					sum -= (*col) * previous[colindex]; 
				}
			}

			next[rowindex] = previous[rowindex] + relaxation_factor * (sum / A[rowindex][rowindex] - previous[rowindex]);
		}

		//extra communication step
		bundle.sendAndRecieveUpdate(next);
		 /*
		if (comm.size() > 1) {
			const double timestart = bundle.timer.elapsed();
			for (size_t i : bundle.ghostrows) {
				next[i] = previous[i];

			}
			const double timeend = bundle.timer.elapsed();
			bundle.timeSpentCopying += timeend - timestart;
		}
		 */

		//second pass
		for (auto row = A.begin(); row != endrow; ++row) {

			const size_t rowindex = row.index();
			if (rowindex <= half){
				continue;
			}
			double sum = b[rowindex];
			auto endcol = (*row).end();
			for (auto col = (*row).begin(); col != endcol; ++col) {

				const size_t colindex = col.index();
				if (rowindex != colindex) {

					sum -= (*col) * next[colindex];
				}
			}

			next[rowindex] = previous[rowindex] + relaxation_factor * (sum / A[rowindex][rowindex] - previous[rowindex]);
		}

		
		double partialsum = norm_of_diff_square(next, previous, ghostbools);
		//compute termination criteria
		termination_criteria = comm.sum(partialsum);

		//cout << termination_criteria << endl;

	} while (termination_criteria > tol*tol);

	bundle.sendAndRecieveUpdate(next);

	if (comm.rank()==0){
		cout << "SOR converged in " << iter << " iterations" << endl;
	}

	return next;
}



Vector sor_rb_mpi(const Matrix &A, const Vector &b, const Vector &x, Bundle &bundle, double relaxation_factor, double tol) {
	//b and x needs to have zeros corresponding to the ghostrows
	Vector previous(x.size()); //zeros
	Vector next(x);  //copy

	ptpComm& comm = bundle.comm;
	auto& ghostbools = bundle.ghostbools;

	//const size_t half = ceil((double)A.N() / 2.0); //if N is odd, there is 1 more red than black
	//const size_t half = A.N() / 2 + A.N() % 2;
	int iter = 0;
	double termination_criteria;
	do {
		iter++;
		
		 /*
		if (iter == 100){
			break;
		}
		 */
		
		//communication stage
		bundle.sendAndRecieveUpdate(next);

		//swap
		next.swap(previous);

		//algorithm
		//first pass
		auto endrow = A.end();
		for (auto row = A.begin(); row != endrow; ++row) {

			const size_t rowindex = row.index();
			//if (rowindex % 2 != 0) { // skip ghostrows
			if (bundle.is_black(rowindex)){
				continue;
			}
			double sum = b[rowindex];
			auto endcol = (*row).end();
			for (auto col = (*row).begin(); col != endcol; ++col) {

				const size_t colindex = col.index();
				if (rowindex != colindex) {

					sum -= (*col) * previous[colindex];
				}
			}

			next[rowindex] = previous[rowindex] + relaxation_factor * (sum / A[rowindex][rowindex] - previous[rowindex]);
		}

		//extra communication step
		//bundle.sendAndRecieveUpdate(next);
		// /*
		if (comm.size() > 1) {
			const double timestart = bundle.timer.elapsed();
			for (size_t i : bundle.ghostrows) {
				next[i] = previous[i];
			}
			const double timeend = bundle.timer.elapsed();
			bundle.timeSpentCopying += timeend - timestart;
		}
		// */

		//second pass
		for (auto row = A.begin(); row != endrow; ++row) {

			const size_t rowindex = row.index();
			if (bundle.is_red(rowindex)) {
				continue;
			}
			double sum = b[rowindex];
			auto endcol = (*row).end();
			for (auto col = (*row).begin(); col != endcol; ++col) {

				const size_t colindex = col.index();
				if (rowindex != colindex) {

					sum -= (*col) * next[colindex];
				}
			}

			next[rowindex] = previous[rowindex] + relaxation_factor * (sum / A[rowindex][rowindex] - previous[rowindex]);
		}


		double partialsum = norm_of_diff_square(next, previous, ghostbools);
		//compute termination criteria
		termination_criteria = comm.sum(partialsum);

		//cout << termination_criteria << endl;

	} while (termination_criteria > tol*tol);

	bundle.sendAndRecieveUpdate(next);

	if (comm.rank() == 0) {
		cout << "SOR RB converged in " << iter << " iterations" << endl;
	}

	return next;
}










