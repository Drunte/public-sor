





#ifndef DOMAIN_SETUP_H
#define DOMAIN_SETUP_H

#include "dune_pch.h"
#include "rhs.h"

#include <dune/common/fvector.hh>
#include <dune/grid/yaspgrid.hh>
#include <dune/grid/common/rangegenerators.hh>


#include <cassert>
#include <array>
#include <set>
#include <map>
#include <vector>





void setupGhostRowsDomain(std::vector<size_t>& ghostrows , const std::vector<std::vector<size_t>>& vec){
	//might have duplicates.
	//use a set.
	std::set<size_t> ghosts;

	for (const auto& v:vec){
		for(const auto& i:v){
			ghosts.insert(i);
		}
	}

	for (const auto& i : ghosts) {
		ghostrows.emplace_back(i);
	}
}


void setupRHSdomain(Vector& b, const size_t xgrid, const size_t ygrid, const Dune::YaspGrid< 2 >::LeafGridView& gridView){
	
	const double dx = 1.0 / (xgrid); //the big, global gridsize
	const double dy = 1.0 / (ygrid);
	b = 0.0;
	//for (const auto &entity : Dune::elements(gridView)) {
	for (const auto &entity : Dune::elements(gridView, Dune::Partitions::interiorBorder)){
		auto center = entity.geometry().center();
		double x = center[0];
		double y = center[1];
		auto index = gridView.indexSet().index(entity);
		
		b[index] = dx * dy * rhs(x, y);
	}
}


/*

void setupMatrixByDomain(Matrix &A, const std::vector<bool> &ghostbools, size_t xgrid, size_t local_interior_size, size_t dim) {
	const double diag = 4.0;
	const double offdiag = -1.0;

	const size_t diff = xgrid - local_interior_size;
	//true if ghostrows exist at location
	const bool bottom_ghosts = !ghostbools[1];
	const bool top_ghosts = !ghostbools[dim - 2];
	const bool left_ghosts = bottom_ghosts ? !ghostbools[local_interior_size + 1] : !ghostbools[0];
	const bool right_ghosts = top_ghosts ? !ghostbools[dim - 2 - local_interior_size] : !ghostbools[dim - 1];

	using std::cout;
	using std::endl;

	cout << "top: " << top_ghosts << endl;
	cout << "left: " << left_ghosts << endl;
	cout << "right: " << right_ghosts << endl;
	cout << "bottom: " << bottom_ghosts << endl;



	//matrix setup
	const size_t start = 0;
	const size_t end = start + dim;

	for (size_t i = start; i < end; i++) {
		if (ghostbools[i]) { 
			//interior cell

			//center
			A.entry(i, i) = diag;

			//east
			if (i + 1 < dim) {
				if(right_ghosts  )
				A.entry(i, i + 1) = offdiag;
			}

			//north
			if (i + xgrid < dim) {
				A.entry(i, i + xgrid) = offdiag; //this could be i+xgrid+ 1 or 2 depending on ghost rows on the left/right of the grid
			}

			//west
			if (i > 0 && i % xgrid > 0) {
				A.entry(i, i - 1) = offdiag;
			}

			//south
			if (i >= xgrid) {
				A.entry(i, i - xgrid) = offdiag;
			}
		}
		else {
			//ghostrow
			A.entry(i, i) = 1.0;
		}
	}

	A.compress();
}
*/

template<class SP>
void setup_matrix(Matrix &A, const SP &sp) {
	//matrix setup
	const size_t totalsize = sp.size();
	A.setBuildMode(Matrix::implicit);	
	A.setImplicitBuildModeParameters(5, 1.0);
	A.setSize(totalsize, totalsize);
	

	const double diag = 4.0;
	const double offdiag = -1.0;

	const size_t start = 0;
	const size_t end = sp.size();

	for (size_t i = start; i < end; i++){
		const auto &columns = sp[i];

		if (columns.empty()) {
			//ghostcell
			A.entry(i, i) = 1.0;
		}
		else {
			for (const auto &col : columns) {
				if (col == i) {
					A.entry(i, i) = diag;
				}
				else {
					A.entry(i, col) = offdiag;
				}
			}
		}
	}
	A.compress();
}



// --LinkBuilder
template< class IndexSet, class IndexMapVector >
class LinkBuilder
 : public Dune::CommDataHandleIF< LinkBuilder< IndexSet, IndexMapVector>, size_t >
{
public:
  // type of set of links
  typedef std :: set< int > LinkStorageType;
  typedef std::map< size_t, std::set< size_t > > IndexMapType;

  typedef IndexMapVector IndexMapVectorType;

  typedef size_t DataType;

protected:
  ptpComm& comm_;
  const IndexSet& indexSet_;

  const DataType myRank_;
  const DataType mySize_;

  LinkStorageType links_;

  IndexMapType sendIndexMap_;
  IndexMapType recvIndexMap_;

  IndexMapVectorType &sendIndexVec_;
  IndexMapVectorType &recvIndexVec_;

public:
  LinkBuilder( ptpComm &comm,
               const IndexSet& indexSet,
               IndexMapVectorType &sendIdxMap,
               IndexMapVectorType &recvIdxMap )
  : comm_( comm ),
    indexSet_( indexSet ),
    myRank_( comm.rank() ),
    mySize_( comm.size() ),
    links_(),
    sendIndexMap_(),
    recvIndexMap_(),
    sendIndexVec_( sendIdxMap ),
    recvIndexVec_( recvIdxMap )
  {}

protected:
  void sendBackSendMaps()
  {
    // symmetric linkage
    LinkStorageType recvLinks( links_ );

    // build linkage
    comm_.removeLinkage();

    // insert new linkage
    comm_.insertRequest( links_, recvLinks );

    // get number of links
    const int nlinks = comm_.sendLinks();

    std::vector< MsgBuffer > buffers( nlinks );

    //////////////////////////////////////////////////////////////
    //
    //  at this point complete send maps exist on receiving side,
    //  so send them back to sending side
    //
    //////////////////////////////////////////////////////////////

    // get destination ranks
    const auto& dest = comm_.sendDest();

    // write all send maps to buffer
    for(int link=0; link<nlinks; ++link)
    {
      const auto& indices = sendIndexMap_[ dest[link] ];
      auto& buffer = buffers[ link ];
      buffer.write( indices.size() );
      for( size_t idx : indices )
      {
        buffer.write( idx );
      }
    }

    // exchange data
    buffers = comm_.exchange( buffers );

    const auto& source = comm_.recvSource();

    // read all send maps from buffer
    // only store indices that actually had been receive on the receiving side
    for(int link=0; link<nlinks; ++link)
    {
      auto& indices = sendIndexMap_[ source[link] ];
      auto& buffer = buffers[ link ];
      size_t size = 0;
      buffer.read( size );
      indices.clear();
      for( size_t i=0; i<size; ++i )
      {
        size_t idx;
        buffer.read( idx );
        indices.insert( idx );
      }
    }

    // fill vector data structures
    fillVector( sendIndexMap_, sendIndexVec_ );
    fillVector( recvIndexMap_, recvIndexVec_ );
  }

  void fillVector( const IndexMapType& idxMap, IndexMapVectorType& idxVec )
  {
	  idxVec.resize(comm_.sendLinks());
	  
    for( const auto& item: idxMap )
    {
      const size_t rank = item.first;
      const auto& idxSet = item.second;
      auto& indices = idxVec[ comm_.sendLink(rank) ];
      indices.clear();
      indices.reserve( idxSet.size() );
      for( const size_t idx : idxSet )
        indices.push_back( idx );
    }
  }
public:
  //! desctructor
  ~LinkBuilder()
  {
    sendBackSendMaps();
  }

 //! returns true if combination is contained
  bool contains( int dim, int codim ) const
  {
    return codim == 0;
  }

  //! return whether we have a fixed size
  bool fixedSize( int dim, int codim ) const
  {
    return true;
  }

  //! read buffer and apply operation
  template< class MessageBuffer, class Entity >
  void gather( MessageBuffer &buffer, const Entity &entity ) const
  {
    // check whether we are a sending entity
    DataType writeData = (entity.partitionType() == Dune::InteriorEntity) ? 1 : 0;
    buffer.write( writeData );

    // if we send data then send rank and dofs
    if( writeData )
    {
      // send rank for linkage
      buffer.write( myRank_ );

      // write index of entity
      DataType idx = indexSet_.index( entity );
      buffer.write( idx );
    }
    else
    {
      DataType dummy = std::numeric_limits< DataType > :: max();
      buffer.write( dummy );
      buffer.write( dummy );
    }
  }

  //! read buffer and apply operation
  template< class MessageBuffer, class Entity >
  void scatter( MessageBuffer &buffer, const Entity &entity, const size_t dataSize )
  {
    DataType hasData;
    buffer.read( hasData );

    if( hasData > 1 ) // should be 0 or 1
    {
      std::cerr << "ERROR: communication problem." << std::endl;
      std::abort();
    }

    // if data size > 0 then other side is sender
    if( hasData == 1 )
    {
      // read rank of other side
      DataType rank;
      buffer.read( rank );
      assert( rank < mySize_ );

      // insert send dof (to be send back)
      {
        DataType sentIdx;
        buffer.read( sentIdx );

        // note the rank that we are connected to
        links_.insert( rank );

        sendIndexMap_[ rank ].insert( sentIdx );
      }

      // insert receiving dofs (number of this entity)
      {
        DataType recvIdx = indexSet_.index( entity );
        recvIndexMap_[ rank ].insert( recvIdx );
      }
    }
    else
    {
      // read two dummy data
      DataType dummy;
      buffer.read( dummy );
      buffer.read( dummy );
    }
  }

  //! return local dof size to be communicated
  template< class Entity >
  size_t size( const Entity &entity ) const
  {
    return 3;
  }
};


template <int dim>
size_t setupSendRecvIndices(ptpComm &comm, const size_t xgrid, const size_t ygrid, Vector &b, Matrix &A, size_t &dimention,
							size_t &local_grid_width, size_t &local_interior_size, 
                          std::vector<std::vector<size_t>> &sendindicesVec, std::vector<std::vector<size_t>> &recvindicesVec )
{
  typedef Dune::YaspGrid< dim > Grid;

  typedef Dune::FieldVector< double, dim > Coordinate;

  Coordinate upper(1.);

  std::array<int, dim> cells;
  std::fill( cells.begin(), cells.end(), xgrid );

  // create Cartesian grid [0,1]^dim with xgrid cells in each direction
  Grid grid(upper, cells);

  //typedef std::map< int, std::vector< size_t > > IndexMapVector;
  typedef std::vector<std::vector<size_t>> IndexMapVector;
  //IndexMapVector sendIndexMap;
  //IndexMapVector recvIndexMap;

  typedef typename Grid :: LeafGridView LeafGridView;
  typedef typename LeafGridView :: IndexSet IndexSet;

  auto gridView = grid.leafGridView();
  const auto& idxSet = gridView.indexSet();

  typedef LinkBuilder< IndexSet, IndexMapVector > LinkBuilderType;

  {
    LinkBuilderType lb( comm, idxSet, sendindicesVec, recvindicesVec);
    grid.communicate( lb, Dune::InteriorBorder_All_Interface, Dune::ForwardCommunication );
  }

  

  // &entity
  //#pragma GCC diagnostic push
  //#pragma GCC diagnostic ignored "-Wunused-variable"
  
  bool searching = true;
  double first_x = -1.0;   //error state
  size_t first_index = std::numeric_limits<std::size_t>::max();  //error state
  size_t nCells = 0;
  std::vector<std::set<size_t>> sparsity_pattern(gridView.indexSet().size(0));
  //std::vector<int> is_black(sparsity_pattern.size(), -1); //might be needed
  for (const auto &entity : Dune::elements(gridView, Dune::Partitions::interiorBorder)) {
	  auto center = entity.geometry().center();
	  double x = center[0];
	  auto index = gridView.indexSet().index(entity);
	  /*
	  if (is_black[index] < 0) {
		  is_black[index] = 1;
	  }*/
	  //const int cell_is_black = is_black[index];
	  std::set<size_t> &row = sparsity_pattern[index];

	  row.insert(index);
	  for (const auto& is :
		  Dune::intersections(gridView, entity)){

		  if (is.neighbor()){

			  const auto& neighbor = is.outside();
			  auto index_n = gridView.indexSet().index(neighbor);
			  row.insert(index_n);
			  //is_black[index_n] = 1 - cell_is_black;
		  }
	  }
	  
		
	  if (nCells == 0) {
		  first_x = x;
		  first_index = index;
	  }
		
	  if (searching && nCells > 0 && std::abs(x - first_x) < 1.0e-12) { 
		  searching = false;
		  local_grid_width = index - first_index; //nCells used to be here
		  local_interior_size = nCells;
	  }
  
	  ++nCells;
  }
  //#pragma GCC diagnostic pop
  
  if (first_x < 0) exit(2);
  if (searching) exit(3);
  if (first_index == std::numeric_limits<std::size_t>::max()) exit(4);
	/*
  std::set<size_t> ghosts;

  for (const auto& v : recvindicesVec) {
	  for (const auto& i : v) {
		  ghosts.insert(i);
	  }
  }
  */
  //might differ from sp.size
  const size_t totalsize = sparsity_pattern.size();
  
	dimention=totalsize;
  b.resize(totalsize);

  setupRHSdomain(b, xgrid, ygrid, gridView);

  setup_matrix(A, sparsity_pattern);
  //std::cout << nCells << std::endl;

  return nCells;
 
}






















#endif // end DOMAIN_SETUP_H





